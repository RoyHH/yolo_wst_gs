import argparse
import json

from torch.utils.data import DataLoader

from models import *
from utils.datasets import *
from utils.utils import *

from Module_Store import *
# wst_check_w_file = 'wst_check_w.txt'
# wst_check_file = 'wst_check_w%.txt'
# wst_check_wij_file = 'wst_check_wij.txt'
#
# WST_module_name = 'Single'
# '''
# 包含：Single, Same, None
# 说明：
#     Single: WST Head 单独存在
#     Same: WST Head 与 yolo Head 同一个
#     None: 没有 WST 功能时，原 YOLO
# '''
#
# test_module_name = 'v3'
# '''
# 当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
# 说明：
#     v1: L1, L2, Cosine of v1
#     v1_cosdis: Cosdis of v1
#     v3: L1, L2, Cosine of v3
#     v3_cosdis: Cosdis of v3
# 当 WST Head 与 yolo Head 同一时，包含：s_v1, s_v3
# 说明：
#     s_v1: L1, L2, Cosine of v1
#     s_v3: L1, L2, Cosine of v3
# '''
#
# WST_Metric_module = 'Better'


def check_result_write(epoch_num, nc, file, wst_cw01, wst_cw10):
    with open(file, 'a') as f:
        if epoch_num == 0:
            f.seek(0)
            f.truncate()
        f.write('***************************** epoch ' +
                str(epoch_num) + ' *****************************' + '\n')
        f.write('#[wst_cw01]# =' + '\n')
        for wi in range(nc):
            for wj in range(nc):
                f.write('%10.6g' % (wst_cw01[wi, wj]))
            f.write('\n')
        f.write('#[wst_cw10]# =' + '\n')
        for wii in range(nc):
            for wjj in range(nc):
                f.write('%10.6g' % (wst_cw10[wii, wjj]))
            f.write('\n')

def check_result_f_write(epoch_num, nc, file, wst_c0, wst_cw01, wst_c1, wst_cw10):
    with open(file, 'a') as f:
        if epoch_num == 0:
            f.seek(0)
            f.truncate()
        f.write('***************************** epoch '
                + str(epoch_num) + ' *****************************' + '\n')
        f.write('#[wst_c0]# =' + '\n')
        for wsi in range(nc):
            for wsj in range(nc):
                if wst_c0[wsi, wsj] != 0:
                    wst_c0[wsi, wsj] = wst_cw01[wsi, wsj] / wst_c0[wsi, wsj]
                f.write('%10.4g' % (wst_c0[wsi, wsj]))
            f.write('\n')
        f.write('#[wst_c1]# =' + '\n')
        for wsii in range(nc):
            for wsjj in range(nc):
                if wst_c1[wsii, wsjj] != 0:
                    wst_c1[wsii, wsjj] = wst_cw10[wsii, wsjj] / wst_c1[wsii, wsjj]
                f.write('%10.4g' % (wst_c1[wsii, wsjj]))
            f.write('\n')


def test(cfg,
         data,
         weights=None,
         batch_size=16,
         imgsz=416,
         conf_thres=0.001,
         iou_thres=0.6,  # for nms
         save_json=False,
         single_cls=False,
         augment=False,
         model=None,
         dataloader=None,
         multi_label=True,
         epoch_num=None,
         epochs=None,
         cf_means=None,
         cf_move=None,
         fe_means=None,
         fe_vars=None,
         fe_move=None):
    # Initialize/load model and set device
    if model is None:
        is_training = False
        device = torch_utils.select_device(opt.device, batch_size=batch_size)
        verbose = opt.task == 'test'

        # Remove previous
        for f in glob.glob('test_batch*.jpg'):
            os.remove(f)

        # Initialize model
        model = Darknet(cfg, imgsz)

        # Load weights
        attempt_download(weights)
        if weights.endswith('.pt'):  # pytorch format
            model.load_state_dict(torch.load(weights, map_location=device)['model'])
        else:  # darknet format
            load_darknet_weights(model, weights)

        # Fuse
        model.fuse()
        model.to(device)

        if device.type != 'cpu' and torch.cuda.device_count() > 1:
            model = nn.DataParallel(model)
    else:  # called by train.py
        is_training = True
        device = next(model.parameters()).device  # get model device
        verbose = False

    # Configure run
    data = parse_data_cfg(data)
    nc = 1 if single_cls else int(data['classes'])  # number of classes
    path = data['valid']  # path to test images
    names = load_classes(data['names'])  # class names
    iouv = torch.linspace(0.5, 0.95, 10).to(device)  # iou vector for mAP@0.5:0.95
    iouv = iouv[0].view(1)  # comment for mAP@0.5:0.95
    niou = iouv.numel()

    # Dataloader
    if dataloader is None:
        dataset = LoadImagesAndLabels(path, imgsz, batch_size, rect=True, single_cls=opt.single_cls, pad=0.5)
        batch_size = min(batch_size, len(dataset))
        dataloader = DataLoader(dataset,
                                batch_size=batch_size,
                                num_workers=min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8]),
                                pin_memory=True,
                                collate_fn=dataset.collate_fn)

    seen = 0
    model.eval()
    _ = model(torch.zeros((1, 3, imgsz, imgsz), device=device)) if device.type != 'cpu' else None  # run once
    coco91class = coco80_to_coco91_class()


    # s = ('%20s' + '%10s' * 6) % ('Class', 'Images', 'Targets', 'P', 'R', 'mAP@0.5', 'F1')
    # p, r, f1, mp, mr, map, mf1, t0, t1 = 0., 0., 0., 0., 0., 0., 0., 0., 0.
    if WST_module_name == 'None' or WST_module_name == 'Same':
        s = ('%20s' + '%10s' * 8) % ('Class', 'Images', 'Targets', 'P', 'R', 'mAP@0.5', 'F1', 'y_ac', 'y_ac_c')
        p, r, f1, mp, mr, map, mf1, epoch_acc, epoch_acc_c, t0, t1 = 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.
    elif WST_module_name == 'Single':
        ''' =============== WST Learning——评价模块、ACC =============== '''  # 2
        '''~~~~~~ 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 2
        if WST_Metric_module == 'Normal':
            s = ('%20s' + '%10s' * 13) % ('Class', 'Images', 'Targets', 'P', 'R', 'mAP@0.5', 'F1', 'y_ac', 'y_ac_c',
                                          'w_acsd', 'w_p0', 'w_p1', 'w_r0', 'w_r1')
            p, r, f1, mp, mr, map, mf1, epoch_acc, epoch_acc_c, \
            epoch_wst_acc, epoch_wst_precise0, epoch_wst_precise1, epoch_wst_recall0, epoch_wst_recall1, t0, t1 = \
                0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.
        elif WST_Metric_module == 'Better':
            s = ('%20s' + '%10s' * 10) % ('Class', 'Images', 'Targets', 'P', 'R', 'mAP@0.5', 'F1', 'y_ac', 'y_ac_c',
                                          'w_1w_ac', 'w_1r_ac')
            p, r, f1, mp, mr, map, mf1, epoch_acc, epoch_acc_c, \
            epoch_wst_1wronge_acc, epoch_wst_1right_acc, t0, t1 = 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.
        ''' ====== end ====== '''


    if WST_module_name == 'None':
        loss = torch.zeros(3, device=device)
    elif WST_module_name == 'Single' or WST_module_name == 'Same':
        ''' =============== WST Learning——Loss =============== '''  # 1
        '''~~~~~~ Loss： yolo + wst ~~~~~~'''  # 1
        loss = torch.zeros(4, device=device)
        ''' ====== end ====== '''


    ''' =============== WST Learning——评价模块、ACC、Check Out =============== '''  # 19
    '''====== 评价模块 + ACC： every epoch —— wst Head ======'''  # 9
    dy0_right_sum = 0.0
    dy0_wrong_sum = 0.0
    dy1_right_sum = 0.0
    dy1_wrong_sum = 0.0
    epoch_wst_precise0 = 0.0
    epoch_wst_precise1 = 0.0
    epoch_wst_recall0 = 0.0
    epoch_wst_recall1 = 0.0
    epoch_wst_acc = 0.0
    epoch_wst_1wronge_acc = 0.0
    epoch_wst_1right_acc = 0.0
    '''====== ACC： every epoch —— yolo Head======'''  # 4
    c0_rsum, c0_rsum_c = 0.0, 0.0
    c0_wsum, c0_wsum_c = 0.0, 0.0
    c1_rsum, c1_rsum_c = 0.0, 0.0
    c1_wsum, c1_wsum_c = 0.0, 0.0
    '''====== Check Out ======'''  # 6
    wst_cw01 = np.zeros(shape=(nc, nc), dtype=int)
    wst_cw10 = np.zeros(shape=(nc, nc), dtype=int)
    wst_cw01_ij = np.zeros(shape=(nc, nc), dtype=int)
    wst_cw10_ij = np.zeros(shape=(nc, nc), dtype=int)
    wst_c0 = np.zeros(shape=(nc, nc), dtype=float)
    wst_c1 = np.zeros(shape=(nc, nc), dtype=float)
    ''' ====== end ====== '''


    jdict, stats, ap, ap_class = [], [], [], []
    for batch_i, (imgs, targets, paths, shapes) in enumerate(tqdm(dataloader, desc=s)):
        imgs = imgs.to(device).float() / 255.0  # uint8 to float32, 0 - 255 to 0.0 - 1.0
        targets = targets.to(device)
        nb, _, height, width = imgs.shape  # batch size, channels, height, width
        whwh = torch.Tensor([width, height, width, height]).to(device)

        # Disable gradients
        with torch.no_grad():
            # Run model
            t = torch_utils.time_synchronized()
            if WST_module_name == 'Same' or WST_module_name == 'None':
                inf_out, train_out = model(imgs, augment=augment)  # inference and training outputs
            elif WST_module_name == 'Single':
                inf_out, train_out, wst_inf_out, wst_train_out = model(imgs, augment=augment)  # inference and training outputs
            t0 += torch_utils.time_synchronized() - t

            # Compute loss
            # if is_training:  # if model has loss hyperparameters
            #     loss += compute_loss(train_out, targets, model)[1][:3]  # GIoU, obj, cls
            if is_training:
                if WST_module_name == 'None':
                    loss += compute_loss(train_out, targets, model, epoch_num)[1][:3]  # GIoU, obj, cls
                    ''' =============== WST Learning——Compute Loss =============== '''  # 2
                elif WST_module_name == 'Same':
                    '''~~~~~~ Compute Loss： yolo + wst ~~~~~~'''  # 2
                    loss += compute_loss(train_out, targets, model, epoch_num)[1][:4]  # GIoU, obj, cls, wst

                elif WST_module_name == 'Single':
                    '''~~~~~~ Compute Loss： yolo + wst Head(单独) ~~~~~~'''  # 2
                    loss += compute_loss_WH(wst_train_out, train_out, targets, model, epoch_num, epochs,
                                            cf_means, cf_move, fe_means, fe_vars, fe_move, is_train=False)[1][:4]  # GIoU, obj, cls, wst

            if WST_module_name == 'Single':
                ''' =============== WST Learning——Test模块 =============== '''  # 2
                '''****** wst Head(单独) ******'''
                if test_module_name == 'v3':
                    '''~~~~~~ 评价模块： v3 类别均值向量全局差值 ~~~~~~'''  # 1
                    wst_cw01, wst_cw10, wst_c0, wst_c1, wst_cw01_ij, wst_cw10_ij, dy0_right, dy0_wrong, dy1_right, dy1_wrong = \
                        wst_v3_test_WH(wst_train_out, targets, model, epoch_num, epochs, cf_means, cf_move, fe_means,
                                       fe_vars, fe_move, wst_cw01, wst_cw10, wst_c0, wst_c1, wst_cw01_ij, wst_cw10_ij)
                ''' ====== end ====== '''

                ''' =============== WST Learning——评价模块、ACC =============== '''  # 5
                '''====== 评价模块 + ACC： every epoch —— wst Head ======'''  # 4
                dy0_right_sum += dy0_right
                dy0_wrong_sum += dy0_wrong
                dy1_right_sum += dy1_right
                dy1_wrong_sum += dy1_wrong


            '''====== ACC： every epoch —— yolo Head======'''  # 1
            c0_rsum, c0_wsum, c1_rsum, c1_wsum, c0_rsum_c, c0_wsum_c, c1_rsum_c, c1_wsum_c = \
                check_acc(train_out, targets, model, c0_rsum, c0_wsum, c1_rsum, c1_wsum,
                          c0_rsum_c, c0_wsum_c, c1_rsum_c, c1_wsum_c)
            ''' ====== end ====== '''


            # Run NMS
            t = torch_utils.time_synchronized()
            output = non_max_suppression(inf_out, conf_thres=conf_thres, iou_thres=iou_thres, multi_label=multi_label)
            t1 += torch_utils.time_synchronized() - t

        # Statistics per image
        for si, pred in enumerate(output):
            labels = targets[targets[:, 0] == si, 1:]
            nl = len(labels)
            tcls = labels[:, 0].tolist() if nl else []  # target class
            seen += 1

            if pred is None:
                if nl:
                    stats.append((torch.zeros(0, niou, dtype=torch.bool), torch.Tensor(), torch.Tensor(), tcls))
                continue

            # Append to text file
            # with open('test.txt', 'a') as file:
            #    [file.write('%11.5g' * 7 % tuple(x) + '\n') for x in pred]

            # Clip boxes to image bounds
            clip_coords(pred, (height, width))

            # Append to pycocotools JSON dictionary
            if save_json:
                # [{"image_id": 42, "category_id": 18, "bbox": [258.15, 41.29, 348.26, 243.78], "score": 0.236}, ...
                # image_id = int(Path(paths[si]).stem.split('_')[-1])
                '''~~~~~~ For IGBT Data ~~~~~~'''  # 3
                id_aa = Path(paths[si]).stem.split('_')[1]
                id_bb = Path(paths[si]).stem.split('_')[2]
                image_id = int(id_aa + id_bb)

                box = pred[:, :4].clone()  # xyxy
                scale_coords(imgs[si].shape[1:], box, shapes[si][0], shapes[si][1])  # to original shape
                box = xyxy2xywh(box)  # xywh
                box[:, :2] -= box[:, 2:] / 2  # xy center to top-left corner
                for p, b in zip(pred.tolist(), box.tolist()):
                    jdict.append({'image_id': image_id,
                                  'category_id': coco91class[int(p[5])],
                                  'bbox': [round(x, 3) for x in b],
                                  'score': round(p[4], 5)})

            # Assign all predictions as incorrect
            correct = torch.zeros(pred.shape[0], niou, dtype=torch.bool, device=device)
            if nl:
                detected = []  # target indices
                tcls_tensor = labels[:, 0]

                # target boxes
                tbox = xywh2xyxy(labels[:, 1:5]) * whwh

                # Per target class
                for cls in torch.unique(tcls_tensor):
                    ti = (cls == tcls_tensor).nonzero(as_tuple=False).view(-1)  # target indices
                    pi = (cls == pred[:, 5]).nonzero(as_tuple=False).view(-1)  # prediction indices

                    # Search for detections
                    if pi.shape[0]:
                        # Prediction to target ious
                        ious, i = box_iou(pred[pi, :4], tbox[ti]).max(1)  # best ious, indices

                        # Append detections
                        for j in (ious > iouv[0]).nonzero(as_tuple=False):
                            d = ti[i[j]]  # detected target
                            if d not in detected:
                                detected.append(d)
                                correct[pi[j]] = ious[j] > iouv  # iou_thres is 1xn
                                if len(detected) == nl:  # all targets already located in image
                                    break

            # Append statistics (correct, conf, pcls, tcls)
            stats.append((correct.cpu(), pred[:, 4].cpu(), pred[:, 5].cpu(), tcls))

        # Plot images
        if batch_i < 1:
            f = 'test_batch%g_gt.jpg' % batch_i  # filename
            plot_images(imgs, targets, paths=paths, names=names, fname=f)  # ground truth
            f = 'test_batch%g_pred.jpg' % batch_i
            plot_images(imgs, output_to_target(output, width, height), paths=paths, names=names, fname=f)  # predictions

    # Compute statistics
    stats = [np.concatenate(x, 0) for x in zip(*stats)]  # to numpy
    if len(stats):
        p, r, ap, f1, ap_class = ap_per_class(*stats)
        if niou > 1:
            p, r, ap, f1 = p[:, 0], r[:, 0], ap.mean(1), ap[:, 0]  # [P, R, AP@0.5:0.95, AP@0.5]
        mp, mr, map, mf1 = p.mean(), r.mean(), ap.mean(), f1.mean()
        nt = np.bincount(stats[3].astype(np.int64), minlength=nc)  # number of targets per class


        if WST_module_name == 'Single':
            ''' =============== WST Learning——评价模块、ACC =============== '''  # 7
            '''====== 评价模块 + ACC： every epoch —— wst Head ======'''  # 5
            if WST_Metric_module == 'Normal':
                epoch_wst_precise0 = dy0_right_sum / (dy0_right_sum + dy1_wrong_sum)
                epoch_wst_precise1 = dy1_right_sum / (dy1_right_sum + dy0_wrong_sum)
                epoch_wst_recall0 = dy0_right_sum / (dy0_right_sum + dy0_wrong_sum)
                epoch_wst_recall1 = dy1_right_sum / (dy1_right_sum + dy1_wrong_sum)
                epoch_wst_acc = (dy0_right_sum + dy1_right_sum) / \
                                (dy0_right_sum + dy0_wrong_sum + dy1_right_sum + dy1_wrong_sum)
            elif WST_Metric_module == 'Better':
                epoch_wst_1wronge_acc = (dy1_wrong_sum) / \
                                (dy0_right_sum + dy0_wrong_sum + dy1_right_sum + dy1_wrong_sum)
                epoch_wst_1right_acc = (dy1_right_sum) / \
                                (dy0_right_sum + dy0_wrong_sum + dy1_right_sum + dy1_wrong_sum)

        '''====== ACC： every epoch —— yolo Head======'''  # 2
        epoch_acc = (c0_rsum + c1_rsum) / (c0_rsum + c0_wsum + c1_rsum + c1_wsum)
        epoch_acc_c = (c0_rsum_c + c1_rsum_c) / (c0_rsum_c + c0_wsum_c + c1_rsum_c + c1_wsum_c)
        ''' ====== end ====== '''


        ''' =============== WST Learning——Check Result Write Out =============== '''  # 3
        check_result_write(epoch_num, nc, wst_check_w_file, wst_cw01, wst_cw10)
        check_result_f_write(epoch_num, nc, wst_check_file, wst_c0, wst_cw01, wst_c1, wst_cw10)
        '''====== v3 类别均值向量全局差值——专用 ======'''  # 1
        check_result_write(epoch_num, nc, wst_check_wij_file, wst_cw01_ij, wst_cw10_ij)
        ''' ====== end ====== '''


    else:
        nt = torch.zeros(1)


    # Print results
    ''' =============== WST Learning——评价模块、ACC =============== '''  # 2
    if WST_module_name == 'None' or WST_module_name == 'Same':
        pf = '%20s' + '%10.3g' * 8  # print format
        print(pf % ('all', seen, nt.sum(), mp, mr, map, mf1, epoch_acc, epoch_acc_c))
    elif WST_module_name == 'Single':
        '''~~~~~~ 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 2
        if WST_Metric_module == 'Normal':
            pf = '%20s' + '%10.3g' * 13  # print format
            print(pf % ('all', seen, nt.sum(), mp, mr, map, mf1, epoch_acc, epoch_acc_c,
                        epoch_wst_acc, epoch_wst_precise0, epoch_wst_precise1, epoch_wst_recall0, epoch_wst_recall1))
        elif WST_Metric_module == 'Better':
            pf = '%20s' + '%10.3g' * 10  # print format
            print(pf % ('all', seen, nt.sum(), mp, mr, map, mf1, epoch_acc, epoch_acc_c,
                        epoch_wst_1wronge_acc, epoch_wst_1right_acc))
        ''' ====== end ====== '''


    # Print results per class
    if verbose and nc > 1 and len(stats):
        for i, c in enumerate(ap_class):
            print(pf % (names[c], seen, nt[c], p[i], r[i], ap[i], f1[i]))

    # Print speeds
    if verbose or save_json:
        t = tuple(x / seen * 1E3 for x in (t0, t1, t0 + t1)) + (imgsz, imgsz, batch_size)  # tuple
        print('Speed: %.1f/%.1f/%.1f ms inference/NMS/total per %gx%g image at batch-size %g' % t)

    # Save JSON
    # if save_json and map and len(jdict):
    #     print('\nCOCO mAP with pycocotools...')
    #     # imgIds = [int(Path(x).stem.split('_')[-1]) for x in dataloader.dataset.img_files]
    #     '''~~~~~~ For IGBT Data ~~~~~~'''  # 1
    #     imgIds = [int(Path(paths[si]).stem.split('_')[1] + Path(paths[si]).stem.split('_')[2]) for x in dataloader.dataset.img_files]
    #
    #     with open('results.json', 'w') as file:
    #         json.dump(jdict, file)
    #
    #     try:
    #         from pycocotools.coco import COCO
    #         from pycocotools.cocoeval import COCOeval
    #
    #         # https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoEvalDemo.ipynb
    #         cocoGt = COCO(glob.glob('../coco/annotations/instances_val*.json')[0])  # initialize COCO ground truth api
    #         cocoDt = cocoGt.loadRes('results.json')  # initialize COCO pred api
    #
    #         cocoEval = COCOeval(cocoGt, cocoDt, 'bbox')
    #         cocoEval.params.imgIds = imgIds  # [:32]  # only evaluate these images
    #         cocoEval.evaluate()
    #         cocoEval.accumulate()
    #         cocoEval.summarize()
    #         # mf1, map = cocoEval.stats[:2]  # update to pycocotools results (mAP@0.5:0.95, mAP@0.5)
    #     except:
    #         print('WARNING: pycocotools must be installed with numpy==1.17 to run correctly. '
    #               'See https://github.com/cocodataset/cocoapi/issues/356')

    # Return results
    maps = np.zeros(nc) + map
    for i, c in enumerate(ap_class):
        maps[c] = ap[i]


    # return (mp, mr, map, mf1, *(loss.cpu() / len(dataloader)).tolist()), maps
    ''' =============== WST Learning——评价模块、ACC =============== '''  # 1
    if WST_module_name == 'Same' or WST_module_name == 'None':
        '''~~~~~~ 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 1
        return (mp, mr, map, mf1, epoch_acc, epoch_acc_c,
                *(loss.cpu() / len(dataloader)).tolist()), maps
    elif WST_module_name == 'Single':
        '''~~~~~~ 评价模块 + ACC： every epoch —— yolo + wst Head(单独) ~~~~~~'''  # 1
        if WST_Metric_module == 'Normal':
            return (mp, mr, map, mf1, epoch_acc, epoch_acc_c, epoch_wst_acc, epoch_wst_precise0, epoch_wst_precise1, epoch_wst_recall0, epoch_wst_recall1,
                    *(loss.cpu() / len(dataloader)).tolist()), maps, cf_means
        elif WST_Metric_module == 'Better':
            return (mp, mr, map, mf1, epoch_acc, epoch_acc_c, epoch_wst_1wronge_acc, epoch_wst_1right_acc,
                    *(loss.cpu() / len(dataloader)).tolist()), maps, cf_means


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='test.py')
    parser.add_argument('--cfg', type=str, default='cfg/yolov3.cfg', help='*.cfg path')
    parser.add_argument('--data', type=str, default='data/template.data', help='*.data path')
    parser.add_argument('--weights', type=str, default='weights/best.pt', help='weights path')
    parser.add_argument('--batch-size', type=int, default=16, help='size of each image batch')
    parser.add_argument('--img-size', type=int, default=512, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.001, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.6, help='IOU threshold for NMS')
    parser.add_argument('--save-json', action='store_true', help='save a cocoapi-compatible JSON results file')
    parser.add_argument('--task', default='test', help="'test', 'study', 'benchmark'")
    parser.add_argument('--device', default='', help='device id (i.e. 0 or 0,1) or cpu')
    parser.add_argument('--single-cls', action='store_true', help='train as single-class dataset')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    opt = parser.parse_args()
    opt.save_json = opt.save_json or any([x in opt.data for x in ['coco.data', 'coco2014.data', 'coco2017.data']])
    opt.cfg = check_file(opt.cfg)  # check file
    opt.data = check_file(opt.data)  # check file
    print(opt)

    # task = 'test', 'study', 'benchmark'
    if opt.task == 'test':  # (default) test normally
        test(opt.cfg,
             opt.data,
             opt.weights,
             opt.batch_size,
             opt.img_size,
             opt.conf_thres,
             opt.iou_thres,
             opt.save_json,
             opt.single_cls,
             opt.augment)

    elif opt.task == 'benchmark':  # mAPs at 256-640 at conf 0.5 and 0.7
        y = []
        for i in list(range(256, 640, 128)):  # img-size
            for j in [0.6, 0.7]:  # iou-thres
                t = time.time()
                r = test(opt.cfg, opt.data, opt.weights, opt.batch_size, i, opt.conf_thres, j, opt.save_json)[0]
                y.append(r + (time.time() - t,))
        np.savetxt('benchmark.txt', y, fmt='%10.4g')  # y = np.loadtxt('study.txt')
