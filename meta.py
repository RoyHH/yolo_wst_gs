import torch
from copy import *
from torch import nn
from torch import optim
from torch import autograd
from torch.autograd import Variable
from torch.nn import functional as F
import numpy as np
import tqdm
import random
import math
import pandas as pd
import test  # import test.py to get mAP after each epoch
from models import *
from utils.datasets import *
from utils.utils import *
from metatrain import *

# mixed_precision = True
# try:  # Mixed precision training https://github.com/NVIDIA/apex
#     from apex import amp
# except:
#     print('metatrain——Apex recommended for faster mixed precision training: https://github.com/NVIDIA/apex')
#     mixed_precision = False  # not installed
mixed_precision = False


class MetaLearner(nn.Module):
    def __init__(self, sum_grads_pi, A, B, Scout_lines):
        super(MetaLearner, self).__init__()
        self.Scout_lines = Scout_lines
        self.sum_grads_pi = sum_grads_pi
        #self.dummy_loss = dummy_loss
        self.model_A = A
        self.model_B = B
        #self.optimizer = optim.SGD(self.model_B.parameters(), lr=beta)

    def parameters(self):
        """
        Override this function to return only net parameters for MetaLearner's optimize
        it will ignore model_A network parameters.
        :return:a
        """
        return self.model_B.parameters()

    '''
    def write_grads(self, sum_grads_pi):
        """
        write loss into learner.model_B, gradients come from sum_grads_pi.
        Since the gradients info is not calculated by general backward, we need this function to write the right gradients
        into model_B network and update model_B parameters as wished.
        :param dummy_loss: dummy loss, nothing but to write our gradients by hook
        :param sum_grads_pi: the summed gradients
        :return:
        """

        # Register a hook on each parameter in the net that replaces the current dummy grad
        # with our grads accumulated across the meta-batch

        hooks = []

        for i, v in enumerate(self.parameters()):
            def closure():
                ii = i
                return lambda grad: sum_grads_pi[ii]

            # if you write: hooks.append( v.register_hook(lambda grad : sum_grads_pi[i]) )
            # it will pop an ERROR, i don't know why?
            hooks.append(v.register_hook(closure()))
        #print('hooks =', hooks)

        # use our sumed gradients_pi to update the theta/net network,
        # since our optimizer receive the self.net.parameters() only.
        self.optimizer.zero_grad()
        #self.dummy_loss.requires_grad_(True)
        self.dummy_loss.backward()
        self.optimizer.step()

        # if you do NOT remove the hook, the GPU memory will expode!!!
        for h in hooks:
            h.remove()
            
    def update_model_B(self):
        for m_from, m_to in zip(self.model_A.modules(), self.model_B.modules()):
            if isinstance(m_to, nn.Linear) or isinstance(m_to, nn.Conv2d) or isinstance(m_to, nn.BatchNorm2d):
                m_to.weight.data = m_from.weight.data.clone()
                if m_to.bias is not None:
                    m_to.bias.data = m_from.bias.data.clone()
    '''

    def cal_para_sum_p(self, a, b, p):
        stat_dict_1 = a
        stat_dict_2 = b
        for k, v in stat_dict_2.items():
             if k in stat_dict_1.keys():
                 stat_dict_1[k] = torch.true_divide(v, p) + stat_dict_1[k]
        return stat_dict_1

    def cal_para_sum(self, a, b):
        stat_dict_1 = a
        stat_dict_2 = b
        for k, v in stat_dict_2.items():
            if k in stat_dict_1.keys():
                stat_dict_1[k] += v
        return stat_dict_1

    def cal_para_diff(self, a, b):
        stat_dict_1 = a
        stat_dict_2 = b
        for k, v in stat_dict_2.items():
             if k in stat_dict_1.keys():
                 stat_dict_1[k] -= v

        return stat_dict_1

    def backward(self):

        #print('model_b_1 =', list(self.model_B.parameters()))
        #self.write_grads(sum_grads_pi)
        #self.cal_para_sum(self.model_B.state_dict(), self.sum_grads_pi, self.Scout_lines)
        self.cal_para_sum(self.model_B.state_dict(), self.sum_grads_pi)
        #self.update_model_B()
        #self.trans_model_B()
        #print('model_b_2 =', list(self.model_B.parameters()))
        #print('a_b_diff = ',np.subtract(np.array(a),np.array(b)))
        torch.cuda.empty_cache()


class Learner(nn.Module):
    def __init__(self, start_epoch, epochs, device, dataloader, nb, opt, accumulate, grid_min, grid_max, gs, batch_size,
                 optimizer, scheduler, data, cfg, imgsz_test, testloader, n_burn, tb_writer, best_fitness, dataset, maps,
                 lf, hyp, img_size, model_A, ema_A, model_B, ema_B, Scout_lines, Rcn_epochs, Judg_iter):
        super(Learner, self).__init__()

        self.start_epoch = start_epoch
        self.epochs = epochs
        self.device = device
        self.dataloader = dataloader
        self.nb = nb
        self.opt = opt
        self.accumulate = accumulate
        self.grid_min = grid_min
        self.grid_max = grid_max
        self.gs = gs
        self.batch_size = batch_size
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.data = data
        self.cfg = cfg
        self.imgsz_test = imgsz_test
        self.testloader = testloader
        self.n_burn = n_burn
        self.tb_writer = tb_writer
        self.best_fitness = best_fitness
        self.dataset = dataset
        self.maps = maps
        self.lf = lf
        self.hyp = hyp
        self.img_size = img_size

        self.model_A = model_A
        self.ema_A = ema_A

        if Gradient_Scout_module == 'Done':
            self.iter_number = 0
            self.model_B = model_B
            self.ema_B = ema_B
            self.Scout_lines = Scout_lines
            self.Rcn_epochs = Rcn_epochs
            self.Judg_iter = Judg_iter
            self.grads_pi = None
            self.lines_loss = None
            self.sum_grads_pi = None
            self.dummy_loss = None
            self.meta_result = []


    def update_model_A(self):
        """
        copy parameters from self.model_B -> self.model_A
        :return:
        """
        for m_from, m_to in zip(self.model_B.modules(), self.model_A.modules()):
            if isinstance(m_to, nn.Linear) or isinstance(m_to, nn.Conv2d) or isinstance(m_to, nn.BatchNorm2d):
                m_to.weight.data = m_from.weight.data.clone()
                if m_to.bias is not None:
                    m_to.bias.data = m_from.bias.data.clone()
        #self.model_A.load_state_dict(self.model_B.state_dict())


    def cal_para_sum(self, a, b):
        stat_dict_1 = a
        stat_dict_2 = b
        for k, v in stat_dict_2.items():
            if k in stat_dict_1.keys():
                stat_dict_1[k] += v
        return stat_dict_1


    def cal_para_diff(self, a, b):
        stat_dict_1 = a
        stat_dict_2 = b
        for k, v in stat_dict_2.items():
            if k in stat_dict_1.keys():
                stat_dict_1[k] -= v
        return stat_dict_1


    def compute_meta_loss(self, pred, targets):
        loss, loss_items = compute_loss(pred, targets, self.model_B)
        if not torch.isfinite(loss):
            print('WARNING: non-finite loss, ending training ', loss_items)
        return loss, loss_items


    @torch.no_grad()
    def get_pred_B(self, imgs):
        pred_B = self.model_B(imgs)
        return pred_B


    @torch.no_grad()
    def get_meta_pred(self, imgs):
        meta_pred = self.model_B(imgs)
        return meta_pred


    def Normal_train(self, Single_WST, cf_means=None, cf_move=None, fe_means=None, fe_vars=None, fe_move=None,
                     lines_number=None):
        if Gradient_Scout_module == 'Done' and Single_WST == 'Wait':
            epochs = self.Rcn_epochs
        else:
            epochs = self.epochs
        # epoch ------------------------------------------------------------------
        for epoch in range(self.start_epoch, epochs):
            self.model_A.train()

            # Update image weights (optional)
            if self.dataset.image_weights:
                # w = self.model.class_weights.cpu().numpy() * (1 - self.maps) ** 2  # class weights
                ''' =============================== Meta Learning ========================= model_A'''
                w = self.model_A.class_weights.cpu().numpy() * (1 - self.maps) ** 2  # class weights

                image_weights = labels_to_image_weights(self.dataset.labels, nc=self.nc, class_weights=w)
                self.dataset.indices = random.choices(range(self.dataset.n), weights=image_weights,
                                                      k=self.dataset.n)  # rand weighted idx


            if WST_module_name == 'None':
                mloss = torch.zeros(4).to(self.device)  # mean losses
                print(('\n' + '%10s' * 8) % ('Epoch', 'gpu_mem', 'GIoU', 'obj', 'cls', 'total', 'targets', 'img_size'))
            elif WST_module_name == 'Single' or WST_module_name == 'Same':
                ''' =============== WST Learning——Loss =============== '''  # 2
                '''~~~~~~ Loss： yolo + wst ~~~~~~'''  # 2
                mloss = torch.zeros(5).to(self.device)  # mean losses
                print(('\n' + '%10s' * 9) % (
                'Epoch', 'gpu_mem', 'GIoU', 'obj', 'cls', 'wst', 'total', 'targets', 'img_size'))
                ''' ====== end ====== '''


            pbar = tqdm(enumerate(self.dataloader), total=self.nb)  # progress bar
            # batch -------------------------------------------------------------
            for i, (imgs, targets, paths, _) in pbar:
                ni = i + self.nb * epoch  # number integrated batches (since train start)
                imgs = imgs.to(self.device).float() / 255.0  # uint8 to float32, 0 - 255 to 0.0 - 1.0
                targets = targets.to(self.device)

                # Burn-in
                if ni <= self.n_burn:
                    xi = [0, self.n_burn]  # x interp

                    # self.model.gr = np.interp(ni, xi, [0.0, 1.0])  # giou loss ratio (obj_loss = 1.0 or giou)
                    ''' =============================== Meta Learning ========================= model_A'''
                    self.model_A.gr = np.interp(ni, xi, [0.0, 1.0])  # giou loss ratio (obj_loss = 1.0 or giou)

                    self.accumulate = max(1, np.interp(ni, xi, [1, 64 / self.batch_size]).round())
                    for j, x in enumerate(self.optimizer.param_groups):
                        # bias lr falls from 0.1 to lr0, all other lrs rise from 0.0 to lr0
                        x['lr'] = np.interp(ni, xi, [0.1 if j == 2 else 0.0, x['initial_lr'] * self.lf(epoch)])
                        x['weight_decay'] = np.interp(ni, xi, [0.0, self.hyp['weight_decay'] if j == 1 else 0.0])
                        if 'momentum' in x:
                            x['momentum'] = np.interp(ni, xi, [0.9, self.hyp['momentum']])

                # Multi-Scale
                if self.opt.multi_scale:
                    if ni / self.accumulate % 1 == 0:  #  adjust img_size (67% - 150%) every 1 batch
                        self.img_size = random.randrange(self.grid_min, self.grid_max + 1) * self.gs
                    sf = self.img_size / max(imgs.shape[2:])  # scale factor
                    if sf != 1:
                        ns = [math.ceil(x * sf / self.gs) * self.gs for x in
                              imgs.shape[2:]]  # new shape (stretched to 32-multiple)
                        imgs = F.interpolate(imgs, size=ns, mode='bilinear', align_corners=False)


                # Forward
                # pred = self.model(imgs)
                ''' =============================== Meta Learning ========================= model_A'''
                if WST_module_name == 'Same' or WST_module_name == 'None':
                    pred = self.model_A(imgs)  # 特殊说明：没有 WST 功能时，原 YOLO 就是这个语句求 pred
                elif WST_module_name == 'Single':
                    ''' =============== WST Learning——Forward Out =============== '''  # 1
                    '''~~~~~~ Forward Out： yolo + wst Head(单独) ~~~~~~'''  # 1
                    wst_pred, pred = self.model_A(imgs)
                    ''' ====== end ====== '''


                # Loss
                if WST_module_name == 'Same' or WST_module_name == 'None':
                    loss, loss_items = compute_loss(pred, targets, self.model_A, epoch)  # 特殊说明：没有 WST 功能时，原 YOLO 就是这个语句求 loss
                elif WST_module_name == 'Single':
                    ''' =============== WST Learning——Compute Loss =============== '''  # 1
                    '''~~~~~~ Compute Loss： yolo + wst Head(单独) ~~~~~~'''  # 1
                    loss, loss_items = compute_loss_WH(wst_pred, pred, targets, self.model_A, epoch, epochs,
                                                       cf_means, cf_move, fe_means, fe_vars, fe_move)
                    ''' ====== end ====== '''


                if not torch.isfinite(loss):
                    print('WARNING: non-finite loss, ending training ', loss_items)
                    return results

                # Backward
                loss *= self.batch_size / 64  # scale loss


                if Gradient_Scout_module == 'Done' and Single_WST == 'Wait':
                    ''' =============================== Meta Learning ========================= '''
                    if epoch == epochs - 1:
                        self.meta_eval = self.meta_eval + loss
                        self.lines_loss = self.lines_loss + loss


                if mixed_precision:
                    with amp.scale_loss(loss, self.optimizer) as scaled_loss:
                        scaled_loss.backward()
                else:
                    loss.backward()
                # loss.backward()


                # Optimize
                if ni % self.accumulate == 0:
                    self.optimizer.step()
                    self.optimizer.zero_grad()

                    # self.ema.update(self.model)
                    ''' =============================== Meta Learning ========================= model_A'''
                    self.ema_A.update(self.model_A)

                # Print
                mloss = (mloss * i + loss_items) / (i + 1)  # update mean losses
                mem = '%.3gG' % (torch.cuda.memory_cached() / 1E9 if torch.cuda.is_available() else 0)  # (GB)


                if WST_module_name == 'None':
                    s = ('%10s' * 2 + '%10.3g' * 6) % (
                    '%g/%g' % (epoch, epochs - 1), mem, *mloss, len(targets), self.img_size)
                elif WST_module_name == 'Single' or WST_module_name == 'Same':
                    ''' =============== WST Learning——Loss Print =============== '''  # 1
                    '''~~~~~~ Loss Print： yolo + wst ~~~~~~'''  # 1
                    s = ('%10s' * 2 + '%10.3g' * 7) % (
                    '%g/%g' % (epoch, epochs - 1), mem, *mloss, len(targets), self.img_size)
                    ''' ====== end ====== '''


                pbar.set_description(s)

                # Plot
                '''if ni < 1:
                    f = 'train_batch%g.jpg' % i  # filename
                    res = plot_images(images=imgs, targets=targets, paths=paths, fname=f)
                    if tb_writer:
                        tb_writer.add_image(f, res, dataformats='HWC', global_step=epoch)
                        # tb_writer.add_graph(model, imgs)  # add model to tensorboard'''

                # end batch ------------------------------------------------------------------------------------------------

            # Update scheduler
            self.scheduler.step()


            # Process epoch results
            # self.ema.update_attr(self.model)
            ''' =============================== Meta Learning ========================= model_A'''
            self.ema_A.update_attr(self.model_A)


            final_epoch = epoch + 1 == epochs
            if not self.opt.notest or final_epoch:  # Calculate mAP
                # is_coco = any([x in data for x in ['coco.data', 'coco2014.data', 'coco2017.data']]) and model.nc == 80
                is_vol = any([x in self.data for x in ['template.data']]) and self.model_A.nc == 9


                if WST_module_name == 'Same' or WST_module_name == 'None':
                    ''' =============== WST Learning——Test Result =============== '''  # 1
                    '''~~~~~~ Test Result： yolo + wst ~~~~~~'''  # 1
                    results, self.maps = test.test(self.cfg,
                                              self.data,
                                              batch_size=self.batch_size,
                                              imgsz=self.imgsz_test,
                                              model=self.ema_A.ema,
                                              save_json=final_epoch and is_vol,
                                              dataloader=self.testloader,
                                              multi_label=ni > self.n_burn,
                                              epoch_num=epoch)  # 特殊说明：没有 WST 功能时，原 YOLO 就是这个语句求 results, maps
                elif WST_module_name == 'Single':
                    '''~~~~~~ Test Result： yolo + wst Head(单独) ~~~~~~'''  # 1
                    results, maps, cf_means = test.test(self.cfg,
                                                        self.data,
                                                        batch_size=self.batch_size,
                                                        imgsz=self.imgsz_test,
                                                        model=self.ema_A.ema,
                                                        save_json=final_epoch and is_vol,
                                                        dataloader=self.testloader,
                                                        multi_label=ni > self.n_burn,
                                                        epoch_num=epoch,
                                                        epochs=epochs,
                                                        cf_means=cf_means,
                                                        cf_move=cf_move,
                                                        fe_means=fe_means,
                                                        fe_vars=fe_vars,
                                                        fe_move=fe_move)
                    ''' ====== end ====== '''


            # Write
            with open(results_file, 'a') as f:
                # f.write(s + '%10.3g' * 7 % results + '\n')  # P, R, mAP, F1, test_losses=(GIoU, obj, cls)
                ''' =============== WST Learning——Write Out =============== '''  # 1
                '''~~~~~~ Write Out： yolo + wst loss/metric ~~~~~~'''  # 1
                # f.write(s + '%10.3g' * 8 % results + '\n')  # P, R, mAP, F1, test_losses=(GIoU, obj, cls, wst)
                if WST_module_name == 'None':
                    f.write(s + '%10.3g' * 9 % results + '\n')
                    # P, R, mAP, F1, acc, acc_c, test_losses=(GIoU, obj, cls)
                elif WST_module_name == 'Same':
                    f.write(s + '%10.3g' * 10 % results + '\n')
                    # P, R, mAP, F1, acc, acc_c, test_losses=(GIoU, obj, cls, wst)
                elif WST_module_name == 'Single':
                    '''~~~~~~ Write Out： yolo + wst loss/metric 
                            + 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 1
                    if WST_Metric_module == 'Normal':
                        f.write(s + '%10.3g' * 15 % results + '\n')
                        # P, R, mAP, F1, acc, acc_c,
                        # wst_accsd, wst_p0sd, wst_p1sd, wst_r0sd, wst_r1sd,
                        # test_losses=(GIoU, obj, cls, wst)
                    elif WST_Metric_module == 'Better':
                        f.write(s + '%10.3g' * 12 % results + '\n')
                        # P, R, mAP, F1, acc, acc_c,
                        # wst_1wronge_acc, wst_1right_acc,
                        # test_losses=(GIoU, obj, cls, wst)
                    ''' ====== end ====== '''


            if len(self.opt.name) and self.opt.bucket:
                os.system('gsutil cp results.txt gs://%s/results/results%s.txt' % (self.opt.bucket, self.opt.name))


            if Gradient_Scout_module == 'Done' and Single_WST == 'Wait':
                ''' =============================== Meta Learning ========================= '''
                # 记录每一支路各类loss的数值
                if epoch == epochs - 1:
                    self.meta_result = self.meta_result + list(mloss[:-1]) + list(results)
            else:
                # Tensorboard
                if self.tb_writer:
                    if WST_module_name == 'None':
                        tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss',
                                'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                                'metrics/acc', 'metrics/acc_c',
                                'val/giou_loss', 'val/obj_loss', 'val/cls_loss']
                    elif WST_module_name == 'Same':
                        ''' =============== WST Learning——Tensorboard Out =============== '''  # 1
                        '''~~~~~~ Tensorboard Out：  yolo + wst loss/metric ~~~~~~'''  # 1
                        tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                                'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                                'metrics/acc', 'metrics/acc_c',
                                'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
                    elif WST_module_name == 'Single':
                        '''~~~~~~ Write Out： yolo + wst loss/metric 
                                + 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 1
                        if WST_Metric_module == 'Normal':
                            tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                                    'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                                    'metrics/acc', 'metrics/acc_c', 'metrics/wst_accsd',
                                    'metrics/wst_p0sd', 'metrics/wst_p1sd', 'metrics/wst_r0sd', 'metrics/wst_r1sd',
                                    'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
                        elif WST_Metric_module == 'Better':
                            tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                                    'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                                    'metrics/acc', 'metrics/acc_c', 'metrics/wst_1wronge_acc', 'metrics/wst_1right_acc',
                                    'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
                        ''' ====== end ====== '''
                    for x, tag in zip(list(mloss[:-1]) + list(results), tags):
                        self.tb_writer.add_scalar(tag, x, epoch)


            # Update best mAP
            fi = fitness(np.array(results).reshape(1, -1))  # fitness_i = weighted combination of [P, R, mAP, F1]
            if fi > self.best_fitness:
                self.best_fitness = fi

            # Save model
            save = (not self.opt.nosave) or (final_epoch and not self.opt.evolve)
            if save:
                with open(results_file, 'r') as f:  # create checkpoint
                    # ckpt = {'epoch': 0,
                    #         'best_fitness': self.best_fitness,
                    #         'training_results': f.read(),
                    #         'model': self.ema.ema.module.state_dict() if hasattr(self.model, 'module') else self.ema.ema.state_dict(),
                    #         'optimizer': None if final_epoch else self.optimizer.state_dict()}
                    ''' =============================== Meta Learning ========================= model_A'''
                    ckpt = {'epoch': 0,
                            'best_fitness': self.best_fitness,
                            'training_results': f.read(),
                            'model': self.ema_A.ema.module.state_dict() if hasattr(self.model_A,
                                                                                   'module') else self.ema_A.ema.state_dict(),
                            'optimizer': None if final_epoch else self.optimizer.state_dict()}

                # Save last, best and delete
                torch.save(ckpt, last)
                if (self.best_fitness == fi) and not final_epoch:
                    torch.save(ckpt, best)
                del ckpt


            if WST_module_name == 'Single':
                ''' =============== WST Learning——CF_means Out =============== '''  # 10
                with open(file_cf_means, 'a') as f:
                    if epoch == 0:
                        f.seek(0)
                        f.truncate()

                    if Gradient_Scout_module == 'Done' and Single_WST == 'Wait':
                        f.write('**************************Iter_' + str(self.iter_number) + '==> Scout-No.' +
                                str(self.Scout_lines) +': epoch ' + str(epoch) + ' **************************' + '\n')
                        f.write('#[cf_means]# =' + '\n')
                        for nj in range(self.model_A.nc):
                            for wj in range(self.model_A.wst_nc):
                                f.write('%12.5g' % (cf_means[epoch, nj, wj]))
                            f.write('\n')
                    else:
                        f.write('************************** epoch ' + str(epoch) + ' **************************' + '\n')
                        f.write('#[cf_means]# =' + '\n')
                        for nj in range(self.model_A.nc):
                            for wj in range(self.model_A.wst_nc):
                                f.write('%12.5g' % (cf_means[epoch, nj, wj]))
                            f.write('\n')


    def Learn_Gradient_Scout(self, cf_means=None, cf_move=None, fe_means=None, fe_vars=None, fe_move=None):
        if WST_module_name == 'Single':
            ''' =============== WST Learning——wst Head(单独) to Meta Learning Gredient Scout=============== '''  # 5
            # 聚类: cf_means--均值, cf_move--移动幅度
            # cf_means_toGS = torch.zeros((self.epochs, self.model.nc, self.model.wst_nc), device='cuda:0')
            # cf_move_toGS = torch.tensor(0.1, device='cuda:0')
            cf_means_toGS = torch.zeros((self.epochs, self.model_A.nc, self.model_A.wst_nc))
            cf_move_toGS = torch.tensor(0.1)
            # BN: fe_means--移动均值, fe_vars--移动方差, fe_move--移动幅度
            # fe_means_toGS = torch.zeros((self.epochs, self.model.wst_nc), device='cuda:0')
            # fe_vars_toGS = torch.zeros((self.epochs, self.model.wst_nc), device='cuda:0')
            # fe_move_toGS = torch.tensor(0.1, device='cuda:0')
            fe_means_toGS = torch.zeros((self.epochs, self.model_A.wst_nc))
            fe_vars_toGS = torch.zeros((self.epochs, self.model_A.wst_nc))
            fe_move_toGS = torch.tensor(0.1)
            ''' ====== end ====== '''

        for self.iter_number in range(self.Judg_iter):
            self.sum_grads_pi = None
            self.meta_eval = 0
            self.meta_result = []
            line_loss_sum = None
            Single_WST = 'Wait'

            for lines_number in range(self.Scout_lines):
                self.lines_loss = 0
                Learner.update_model_A(self)
                self.grads_pi = None

                Learner.Normal_train(self, Single_WST, cf_means=cf_means_toGS, cf_move=cf_move_toGS,
                                     fe_means=fe_means_toGS, fe_vars=fe_vars_toGS, fe_move=fe_move_toGS,
                                     lines_number=lines_number)

                ''' =============================== Meta Learning ========================= '''
                print('\033[1;36m Scout_No.%g Loss = %g \033[0m' % (lines_number, self.lines_loss))
                # print('lines_number = ', lines_number)
                # print('lines_loss = ', self.lines_loss)

                cf_means_toGS.zero_()
                fe_means_toGS.zero_()
                fe_vars_toGS.zero_()

                # 根据model_A网络参数变化情况，计算本条支路梯度
                #self.grads_pi = np.subtract(np.array(list(self.model_A.parameters())),np.array(list(self.model_B.parameters())))
                self.grads_pi = self.cal_para_diff(deepcopy(self.model_A.state_dict()), deepcopy(self.model_B.state_dict()))

                # 计算多条支路融合梯度
                #self.sum_grads_pi =tuple(self.sum_grads_pi + np.array(grads_pi))
                if self.sum_grads_pi is None:
                    self.sum_grads_pi = self.grads_pi
                    line_loss_sum = self.lines_loss
                else:  # accumulate all gradients from different episode learner
                #self.sum_grads_pi = [torch.add(i, j) for i, j in zip(self.sum_grads_pi, grads_pi)]

                    # 判断本条支路梯度是否融入
                    if line_loss_sum > self.lines_loss:
                        line_loss_sum = (line_loss_sum + self.lines_loss) * 0.5
                        self.sum_grads_pi = self.cal_para_sum(self.sum_grads_pi, self.grads_pi)


            ''' =============================== Meta Learning ========================= '''
            print('\033[1;36m Iter_No.%g: Pacesetter Jugemet Loss = %g \033[0m' % (self.iter_number, line_loss_sum))

            # 根据多支路融合梯度，更新model_B参数
            meta = MetaLearner(self.sum_grads_pi, self.model_A, self.model_B, self.Scout_lines)
            meta.backward()

            self.meta_eval = self.meta_eval / self.Scout_lines

            print('\033[1;36m Iter_No.%g: All Scouts Mean Loss = %g \033[0m' % (self.iter_number, self.meta_eval))

            self.tb_writer.add_scalar('meta_eval', self.meta_eval, self.iter_number)


            if WST_module_name == 'None':
                tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss',
                        'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                        'metrics/acc', 'metrics/acc_c',
                        'val/giou_loss', 'val/obj_loss', 'val/cls_loss']
            elif WST_module_name == 'Same':
                ''' =============== WST Learning——Tensorboard Out =============== '''  # 1
                '''~~~~~~ Tensorboard Out：  yolo + wst loss/metric ~~~~~~'''  # 1
                tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                        'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                        'metrics/acc', 'metrics/acc_c',
                        'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
            elif WST_module_name == 'Single':
                '''~~~~~~ Write Out： yolo + wst loss/metric 
                        + 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 1
                if WST_Metric_module == 'Normal':
                    tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                            'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                            'metrics/acc', 'metrics/acc_c', 'metrics/wst_accsd',
                            'metrics/wst_p0sd', 'metrics/wst_p1sd', 'metrics/wst_r0sd', 'metrics/wst_r1sd',
                            'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
                elif WST_Metric_module == 'Better':
                    tags = ['train/giou_loss', 'train/obj_loss', 'train/cls_loss', 'train/wst_loss',
                            'metrics/precision', 'metrics/recall', 'metrics/mAP_0.5', 'metrics/F1',
                            'metrics/acc', 'metrics/acc_c', 'metrics/wst_1wronge_acc', 'metrics/wst_1right_acc',
                            'val/giou_loss', 'val/obj_loss', 'val/cls_loss', 'val/wst_loss']
                ''' ====== end ====== '''


            ''' =============================== Meta Learning ========================= '''
            self.meta_result = [x/self.Scout_lines for x in self.meta_result]


            for x, tag in zip(self.meta_result, tags):
                self.tb_writer.add_scalar(tag, x, self.iter_number)


        if Multi_GS_to_WST == 'Done':
            print('\n =================================================================================='
                  ' \n\033[1;33m Gradient Scout end ===> WST start \033[0m')
            Learner.update_model_A(self)
            Single_WST = 'Now'
            Learner.Normal_train(self, Single_WST, cf_means=cf_means, cf_move=cf_move,
                                 fe_means=fe_means, fe_vars=fe_vars, fe_move=fe_move)