import argparse
import torch.utils.data
from torch import autograd
import torch.distributed as dist
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from torch.utils.tensorboard import SummaryWriter
from meta import *
import test  # import test.py to get mAP after each epoch
from models import *
from utils.datasets import *
from utils.utils import *

# mixed_precision = True
# try:  # Mixed precision training https://github.com/NVIDIA/apex
#     from apex import amp
# except:
#     print('metatrain——Apex recommended for faster mixed precision training: https://github.com/NVIDIA/apex')
#     mixed_precision = False  # not installed
mixed_precision = False

from Module_Store import *
# wdir = 'weights' + os.sep  # weights dir
# last = wdir + 'last.pt'
# best = wdir + 'best.pt'
# results_file = 'results.txt'
#
# file_cf_means = 'WST_cf_means.txt'
#
# # Hyperparameters
# hyp = {'giou': 3.54,  # giou loss gain
#        'cls': 37.4,  # cls loss gain
#        'cls_pw': 1.0,  # cls BCELoss positive_weight
#        'obj': 64.3,  # obj loss gain (*=img_size/320 if img_size != 320)
#        'obj_pw': 1.0,  # obj BCELoss positive_weight
#        'iou_t': 0.2,  # iou training threshold
#        'lr0': 0.01,  # initial learning rate (SGD=5E-3, Adam=5E-4)
#        'lrf': 0.0005,  # final learning rate (with cos scheduler)
#        'momentum': 0.937,  # SGD momentum
#        'weight_decay': 0.0005,  # optimizer weight decay
#        'fl_gamma': 0.0,  # focal loss gamma (efficientDet default is gamma=1.5)
#        'hsv_h': 0.0138,  # image HSV-Hue augmentation (fraction)
#        'hsv_s': 0.678,  # image HSV-Saturation augmentation (fraction)
#        'hsv_v': 0.36,  # image HSV-Value augmentation (fraction)
#        'degrees': 1.98 * 0,  # image rotation (+/- deg)
#        'translate': 0.05 * 0,  # image translation (+/- fraction)
#        'scale': 0.05 * 0,  # image scale (+/- gain)
#        'shear': 0.641 * 0}  # image shear (+/- deg)
# ''' =============== WST Learning: loss gain,positive_weight =============== '''
# hyp['wst'] = 4.26
# hyp['wst_pw'] = 1.0
# ''' ====== end ====== '''
#
# WST_module_name = 'Single'
# '''
# 包含：Single, Same, None
# 说明：
#     Single: WST Head 单独存在
#     Same: WST Head 与 yolo Head 同一个
#     None: 没有 WST 功能时，原 YOLO
# '''
#
# BN_WST_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做度量前 Feature maps 做 BN 操作
#     None: 在做度量前 Feature maps 不做 BN 操作
# '''
#
# WST_Metric_module = 'Better'
#
# Gradient_Scout_module = 'Done'
#
# Multi_GS_to_WST = 'Done'


# Overwrite hyp with hyp*.txt (optional)


f = glob.glob('hyp*.txt')
if f:
    print('Using %s' % f[0])
    for k, v in zip(hyp.keys(), np.loadtxt(f[0])):
        hyp[k] = v

# Print focal loss if gamma > 0
if hyp['fl_gamma']:
    print('Using FocalLoss(gamma=%g)' % hyp['fl_gamma'])

def learner_train(hyp):
    cfg = opt.cfg
    data = opt.data
    epochs = opt.epochs  # 500200 batches at bs 64, 117263 images = 273 epochs
    R_epochs = opt.Rcn_epochs
    S_lines = opt.Scout_lines
    J_iter = opt.Judg_iter
    batch_size = opt.batch_size
    accumulate = max(round(64 / batch_size), 1)  # accumulate n times before optimizer update (bs 64)
    weights = opt.weights  # initial training weights
    imgsz_min, imgsz_max, imgsz_test = opt.img_size  # img sizes (min, max, test)

    # Image Sizes
    gs = 32  # (pixels) grid size
    assert math.fmod(imgsz_min, gs) == 0, '--img-size %g must be a %g-multiple' % (imgsz_min, gs)
    opt.multi_scale |= imgsz_min != imgsz_max  # multi if different (min, max)
    if opt.multi_scale:
        if imgsz_min == imgsz_max:
            imgsz_min //= 1.5
            imgsz_max //= 0.667
        grid_min, grid_max = imgsz_min // gs, imgsz_max // gs
        imgsz_min, imgsz_max = int(grid_min * gs), int(grid_max * gs)
    img_size = imgsz_max  # initialize with max size

    # Configure run
    init_seeds()
    data_dict = parse_data_cfg(data)
    train_path = data_dict['train']
    test_path = data_dict['valid']
    nc = 1 if opt.single_cls else int(data_dict['classes'])  # number of classes
    hyp['cls'] *= nc / 80  # update coco-tuned hyp['cls'] to current dataset


    if WST_module_name == 'Same':
        wst_nc = nc
    elif WST_module_name == 'Single':
        ''' =============== WST Learning——wst Head(单独) =============== '''  # 1
        wst_nc = 9 * nc
        ''' ====== end ====== '''


    # Remove previous results
    for f in glob.glob('*_batch*.jpg') + glob.glob(results_file):
        os.remove(f)


    # Initialize model
    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= '''
        model_A = Darknet(cfg).to(device)
        model_B = Darknet(cfg).to(device)
    else:
        model = Darknet(cfg).to(device)


    # Optimizer
    pg0, pg1, pg2 = [], [], []  # optimizer parameter group
    # for k, v in dict(model.named_parameters()).items():
    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= model_A'''
        for k, v in dict(model_A.named_parameters()).items():
            if '.bias' in k:
                pg2 += [v]  # biases
            elif 'Conv2d.weight' in k:
                pg1 += [v]  # apply weight_decay
            else:
                pg0 += [v]  # all else
    else:
        for k, v in dict(model.named_parameters()).items():
            if '.bias' in k:
                pg2 += [v]  # biases
            elif 'Conv2d.weight' in k:
                pg1 += [v]  # apply weight_decay
            else:
                pg0 += [v]  # all else


    if opt.adam:
        # hyp['lr0'] *= 0.1  # reduce lr (i.e. SGD=5E-3, Adam=5E-4)
        optimizer = optim.Adam(pg0, lr=hyp['lr0'])
        # optimizer = AdaBound(pg0, lr=hyp['lr0'], final_lr=0.1)
    else:
        optimizer = optim.SGD(pg0, lr=hyp['lr0'], momentum=hyp['momentum'], nesterov=True)
    optimizer.add_param_group({'params': pg1, 'weight_decay': hyp['weight_decay']})  # add pg1 with weight_decay
    optimizer.add_param_group({'params': pg2})  # add pg2 (biases)
    print('Optimizer groups: %g .bias, %g Conv2d.weight, %g other' % (len(pg2), len(pg1), len(pg0)))
    del pg0, pg1, pg2

    start_epoch = 0
    best_fitness = 0.0

    attempt_download(weights)
    if weights.endswith('.pt'):  # pytorch format
        # possible weights are '*.pt', 'yolov3-spp.pt', 'yolov3-tiny.pt' etc.
        ckpt = torch.load(weights, map_location=device)


        # load model
        try:
            if Gradient_Scout_module == 'Done':
                ''' =============================== Meta Learning ========================= model_A'''
                ckpt['model'] = {k: v for k, v in ckpt['model'].items() if model_A.state_dict()[k].numel() == v.numel()}
                model_A.load_state_dict(ckpt['model'], strict=False)
            else:
                ckpt['model'] = {k: v for k, v in ckpt['model'].items() if model.state_dict()[k].numel() == v.numel()}
                model.load_state_dict(ckpt['model'], strict=False)
        except KeyError as e:
            s = "%s is not compatible with %s. Specify --weights '' or specify a --cfg compatible with %s. " \
                "See https://github.com/ultralytics/yolov3/issues/657" % (opt.weights, opt.cfg, opt.weights)
            raise KeyError(s) from e


        # load optimizer
        if ckpt['optimizer'] is not None:
            optimizer.load_state_dict(ckpt['optimizer'])
            best_fitness = ckpt['best_fitness']

        # load results
        if ckpt.get('training_results') is not None:
            with open(results_file, 'w') as file:
                file.write(ckpt['training_results'])  # write results.txt

        # epochs
        start_epoch = ckpt['epoch'] + 1
        if epochs < start_epoch:
            print('%s has been trained for %g epochs. Fine-tuning for %g additional epochs.' %
                  (opt.weights, ckpt['epoch'], epochs))
            epochs += ckpt['epoch']  # finetune additional epochs

        del ckpt


    elif len(weights) > 0:  # darknet format
        # possible weights are '*.weights', 'yolov3-tiny.conv.15',  'darknet53.conv.74' etc.
        if Gradient_Scout_module == 'Done':
            ''' =============================== Meta Learning ========================= model_A'''
            load_darknet_weights(model_A, weights)
            #load_darknet_weights(model_B, weights)
        else:
            load_darknet_weights(model, weights)


    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= model_A'''
        if opt.freeze_layers:
            output_layer_indices = [idx - 1 for idx, module in enumerate(model_A.module_list) if isinstance(module, YOLOLayer)]
            freeze_layer_indices = [x for x in range(len(model_A.module_list)) if
                                    (x not in output_layer_indices) and
                                    (x - 1 not in output_layer_indices)]
            for idx in freeze_layer_indices:
                for parameter in model_A.module_list[idx].parameters():
                    parameter.requires_grad_(False)
        # Mixed precision training https://github.com/NVIDIA/apex
        if mixed_precision:
            model_A, optimizer = amp.initialize(model_A, optimizer, opt_level='O1', verbosity=0)
            model_B, optimizer = amp.initialize(model_B, optimizer, opt_level='O1', verbosity=0)
    else:
        if opt.freeze_layers:
            output_layer_indices = [idx - 1 for idx, module in enumerate(model.module_list) if isinstance(module, YOLOLayer)]
            freeze_layer_indices = [x for x in range(len(model.module_list)) if
                                    (x not in output_layer_indices) and
                                    (x - 1 not in output_layer_indices)]
            for idx in freeze_layer_indices:
                for parameter in model.module_list[idx].parameters():
                    parameter.requires_grad_(False)
        # Mixed precision training https://github.com/NVIDIA/apex
        if mixed_precision:
            model, optimizer = amp.initialize(model, optimizer, opt_level='O1', verbosity=0)


    # Scheduler https://arxiv.org/pdf/1812.01187.pdf
    lf = lambda x: (((1 + math.cos(x * math.pi / epochs)) / 2) ** 1.0) * 0.95 + 0.05  # cosine
    scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)
    scheduler.last_epoch = start_epoch - 1  # see link below
    # https://discuss.pytorch.org/t/a-problem-occured-when-resuming-an-optimizer/28822

    # Plot lr schedule
    # y = []
    # for _ in range(epochs):
    #     scheduler.step()
    #     y.append(optimizer.param_groups[0]['lr'])
    # plt.plot(y, '.-', label='LambdaLR')
    # plt.xlabel('epoch')
    # plt.ylabel('LR')
    # plt.tight_layout()
    # plt.savefig('LR.png', dpi=300)


    # Initialize distributed training
    if device.type != 'cpu' and torch.cuda.device_count() > 1 and torch.distributed.is_available():
        dist.init_process_group(backend='nccl',  # 'distributed backend'
                                init_method='tcp://127.0.0.1:9999',  # distributed training init method
                                world_size=1,  # number of nodes for distributed training
                                rank=0)  # distributed training node rank


        if Gradient_Scout_module == 'Done':
            ''' =============================== Meta Learning ========================= '''
            model_A = torch.nn.parallel.DistributedDataParallel(model_A, find_unused_parameters=True)
            model_A.yolo_layers = model_A.module.yolo_layers  # move yolo layer indices to top level
            model_B = torch.nn.parallel.DistributedDataParallel(model_B, find_unused_parameters=True)
            model_B.yolo_layers = model_B.module.yolo_layers  # move yolo layer indices to top level
        else:
            model = torch.nn.parallel.DistributedDataParallel(model, find_unused_parameters=True)
            model.yolo_layers = model.module.yolo_layers  # move yolo layer indices to top level


    # Dataset
    # dataset = LoadImagesAndLabels(train_path, img_size, batch_size,
    #                               augment=True,
    #                               hyp=hyp,  # augmentation hyperparameters
    #                               rect=opt.rect,  # rectangular training
    #                               cache_images=opt.cache_images,
    #                               single_cls=opt.single_cls)
    dataset = LoadImagesAndLabels(train_path, img_size, batch_size,
                                  augment=True,
                                  hyp=hyp,  # augmentation hyperparameters
                                  rect=opt.rect,  # rectangular training
                                  cache_images=opt.cache_images)

    # Dataloader
    batch_size = min(batch_size, len(dataset))
    # nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  # number of workers
    nw = 8
    ''' pytorch 1.6 '''
    # dataloader = torch.utils.data.DataLoader(dataset,
    #                                          batch_size=batch_size,
    #                                          num_workers=nw,
    #                                          shuffle=not opt.rect,  # Shuffle=True unless rectangular training is used
    #                                          pin_memory=True,
    #                                          collate_fn=dataset.collate_fn)
    ''' pytorch 1.7 '''
    dataloader = torch.utils.data.DataLoader(dataset,
                                             batch_size=batch_size,
                                             num_workers=nw,
                                             shuffle=not opt.rect,  # Shuffle=True unless rectangular training is used
                                             pin_memory=True,
                                             collate_fn=dataset.collate_fn,
                                             prefetch_factor=4)

    # Testloader
    # testloader = torch.utils.data.DataLoader(LoadImagesAndLabels(test_path, imgsz_test, batch_size,
    #                                                              hyp=hyp,
    #                                                              rect=True,
    #                                                              cache_images=opt.cache_images,
    #                                                              single_cls=opt.single_cls),
    #                                          batch_size=batch_size,
    #                                          num_workers=nw,
    #                                          pin_memory=True,
    #                                          collate_fn=dataset.collate_fn)
    ''' pytorch 1.6 '''
    # testloader = torch.utils.data.DataLoader(LoadImagesAndLabels(test_path, imgsz_test, batch_size,
    #                                                              hyp=hyp,
    #                                                              rect=True,
    #                                                              cache_images=opt.cache_images),
    #                                          batch_size=batch_size,
    #                                          num_workers=nw,
    #                                          pin_memory=True,
    #                                          collate_fn=dataset.collate_fn)
    ''' pytorch 1.7 '''
    testloader = torch.utils.data.DataLoader(LoadImagesAndLabels(test_path, imgsz_test, 4,
                                                                 hyp=hyp,
                                                                 rect=True,
                                                                 cache_images=opt.cache_images),
                                             batch_size=4,
                                             num_workers=nw,
                                             pin_memory=True,
                                             collate_fn=dataset.collate_fn,
                                             prefetch_factor=4)


    # Model parameters
    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= '''
        model_A.nc = nc  # attach number of classes to model
        model_B.nc = nc
        model_A.hyp = hyp  # attach hyperparameters to model
        model_B.hyp = hyp
        model_A.gr = 1.0  # giou loss ratio (obj_loss = 1.0 or giou)
        model_B.gr = 1.0
        model_A.class_weights = labels_to_class_weights(dataset.labels, nc).to(device)  # attach class weights
        model_B.class_weights = labels_to_class_weights(dataset.labels, nc).to(device)

        if WST_module_name == 'Single':
            ''' =============== WST Learning——wst Head(单独) =============== '''  # 1
            model_A.wst_nc = wst_nc  # attach number of classes to model
            model_B.wst_nc = wst_nc  # attach number of classes to model
            ''' ====== end ====== '''
    else:
        model.nc = nc  # attach number of classes to model
        model.hyp = hyp  # attach hyperparameters to model
        model.gr = 1.0  # giou loss ratio (obj_loss = 1.0 or giou)
        model.class_weights = labels_to_class_weights(dataset.labels, nc).to(device)  # attach class weights

        if WST_module_name == 'Single':
            ''' =============== WST Learning——wst Head(单独) =============== '''  # 1
            model.wst_nc = wst_nc  # attach number of classes to model
            ''' ====== end ====== '''


    # Model EMA
    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= '''
        ema_A = torch_utils.ModelEMA(model_A)
        ema_B = torch_utils.ModelEMA(model_B)
    else:
        ema = torch_utils.ModelEMA(model)


    # Start training
    nb = len(dataloader)  # number of batches
    n_burn = max(3 * nb, 500)  # burn-in iterations, max(3 epochs, 500 iterations)
    maps = np.zeros(nc)  # mAP per class
    # torch.autograd.set_detect_anomaly(True)


    # results = (0, 0, 0, 0, 0, 0, 0)  # 'P', 'R', 'mAP', 'F1', 'val GIoU', 'val Objectness', 'val Classification'
    if WST_module_name == 'None' or WST_module_name == 'Same':
        results = (0, 0, 0, 0, 0, 0, 0, 0, 0)
        # 'P', 'R', 'mAP', 'F1', 'acc', 'acc_c',
        # 'val GIoU', 'val Objectness', 'val Classification'
    elif WST_module_name == 'Single':
        ''' =============== WST Learning——评价模块、ACC =============== '''  # 1
        '''~~~~~~ 评价模块 + ACC： every epoch —— yolo + wst ~~~~~~'''  # 1
        if WST_Metric_module == 'Normal':
            results = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            # 'P', 'R', 'mAP', 'F1', 'acc', 'acc_c',
            # 'wst_accsd', 'wst_p0sd', 'wst_p1sd', 'wst_r0sd', 'wst_r1sd',
            # 'val GIoU', 'val Objectness', 'val Classification'
        elif WST_Metric_module == 'Better':
            results = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            # 'P', 'R', 'mAP', 'F1', 'acc', 'acc_c',
            # 'wst_1wronge_acc', 'wst_1right_acc',
            # 'val GIoU', 'val Objectness', 'val Classification'
        ''' ====== end ====== '''

    if WST_module_name == 'Single':
        ''' =============== WST Learning——wst Head(单独) =============== '''  # 5
        # 聚类: cf_means--均值, cf_move--移动幅度
        # cf_means = torch.zeros((epochs, nc, wst_nc), device='cuda:0')
        # cf_move = torch.tensor(0.1, device='cuda:0')
        cf_means = torch.zeros((epochs, nc, wst_nc))
        cf_move = torch.tensor(0.1)
        # BN: fe_means--移动均值, fe_vars--移动方差, fe_move--移动幅度
        # fe_means = torch.zeros((epochs, wst_nc), device='cuda:0')
        # fe_vars = torch.zeros((epochs, wst_nc), device='cuda:0')
        # fe_move = torch.tensor(0.1, device='cuda:0')
        fe_means = torch.zeros((epochs, wst_nc))
        fe_vars = torch.zeros((epochs, wst_nc))
        fe_move = torch.tensor(0.1)
        ''' ====== end ====== '''


    t0 = time.time()
    print('Image sizes %g - %g train, %g test' % (imgsz_min, imgsz_max, imgsz_test))
    print('Using %g dataloader workers' % nw)
    print('Starting training for %g epochs...' % epochs)


    if Gradient_Scout_module == 'Done':
        ''' =============================== Meta Learning ========================= '''
        Learn_module = Learner(start_epoch, epochs, device, dataloader, nb, opt, accumulate, grid_min, grid_max, gs,
                               batch_size, optimizer, scheduler, data, cfg, imgsz_test, testloader, n_burn, tb_writer,
                               best_fitness, dataset, maps, lf, hyp, img_size, model_A, ema_A, model_B, ema_B,
                               S_lines, R_epochs, J_iter)
        Learn_module.Learn_Gradient_Scout(cf_means=cf_means, cf_move=cf_move, fe_means=fe_means, fe_vars=fe_vars,
                                          fe_move=fe_move)
        torch.cuda.empty_cache()
        ''' ======== end ======== '''
    else:
        Learn_module = Learner(start_epoch, epochs, device, dataloader, nb, opt, accumulate, grid_min, grid_max, gs,
                               batch_size, optimizer, scheduler, data, cfg, imgsz_test, testloader, n_burn, tb_writer,
                               best_fitness, dataset, maps, lf, hyp, img_size, model, ema)
        Learn_module.Normal_train(cf_means=cf_means, cf_move=cf_move, fe_means=fe_means, fe_vars=fe_vars, fe_move=fe_move)


    n = opt.name
    if len(n):
        n = '_' + n if not n.isnumeric() else n
        fresults, flast, fbest = 'results%s.txt' % n, wdir + 'last%s.pt' % n, wdir + 'best%s.pt' % n
        for f1, f2 in zip([wdir + 'last.pt', wdir + 'best.pt', 'results.txt'], [flast, fbest, fresults]):
            if os.path.exists(f1):
                os.rename(f1, f2)  # rename
                ispt = f2.endswith('.pt')  # is *.pt
                strip_optimizer(f2) if ispt else None  # strip optimizer
                os.system('gsutil cp %s gs://%s/weights' % (f2, opt.bucket)) if opt.bucket and ispt else None  # upload


    if not opt.evolve:
        plot_results()  # save as results.png
    #print('%g epochs completed in %.3f hours.\n' % (epoch - start_epoch + 1, (time.time() - t0) / 3600))
    dist.destroy_process_group() if torch.cuda.device_count() > 1 else None
    torch.cuda.empty_cache()
    return results


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=1)  # 500200 batches at bs 16, 117263 COCO images = 273 epochs
    parser.add_argument('--Rcn_epochs', type=int, default=1)  # 500200 batches at bs 16, 117263 COCO images = 273 epochs
    parser.add_argument('--Scout_lines', type=int, default=2)  # 500200 batches at bs 16, 117263 COCO images = 273 epochs
    parser.add_argument('--Judg_iter', type=int, default=2)  # 500200 batches at bs 16, 117263 COCO images = 273 epochs
    parser.add_argument('--batch-size', type=int, default=3)  # effective bs = batch_size * accumulate = 16 * 4 = 64
    parser.add_argument('--cfg', type=str, default='cfg/yolov3_wst_mdyo.cfg', help='*.cfg path')
    parser.add_argument('--data', type=str, default='data/template.data', help='*.data path')
    parser.add_argument('--multi-scale', action='store_true', help='adjust (67%% - 150%%) img_size every 10 batches')
    parser.add_argument('--img-size', nargs='+', type=int, default=[320, 640], help='[min_train, max-train, test]')
    parser.add_argument('--rect', action='store_true', help='rectangular training')
    parser.add_argument('--resume', action='store_true', help='resume training from last.pt')
    parser.add_argument('--nosave', action='store_true', help='only save final checkpoint')
    parser.add_argument('--notest', action='store_true', help='only test final epoch')
    parser.add_argument('--evolve', action='store_true', help='evolve hyperparameters')
    parser.add_argument('--bucket', type=str, default='', help='gsutil bucket')
    parser.add_argument('--cache-images', action='store_true', help='cache images for faster training')
    parser.add_argument('--weights', type=str, default='weights/darknet53.conv.74', help='initial weights path')
    parser.add_argument('--name', default='', help='renames results.txt to results_name.txt if supplied')
    parser.add_argument('--device', default='cpu', help='device id (i.e. 0 or 0,1 or cpu)')
    parser.add_argument('--adam', action='store_true', help='use adam optimizer')
    parser.add_argument('--single-cls', action='store_true', help='train as single-class dataset')
    parser.add_argument('--freeze-layers', action='store_true', help='Freeze non-output layers')
    opt = parser.parse_args()
    opt.weights = last if opt.resume and not opt.weights else opt.weights
    check_git_status()
    opt.cfg = check_file(opt.cfg)  # check file
    opt.data = check_file(opt.data)  # check file
    print(opt)
    opt.img_size.extend([opt.img_size[-1]] * (3 - len(opt.img_size)))  # extend to 3 sizes (min, max, test)
    device = torch_utils.select_device(opt.device, apex=mixed_precision, batch_size=opt.batch_size)
    if device.type == 'cpu':
        mixed_precision = False

    # scale hyp['obj'] by img_size (evolved at 320)
    # hyp['obj'] *= opt.img_size[0] / 320.

    tb_writer = None
    if not opt.evolve:  # Train normally
        print('Start Tensorboard with "tensorboard --logdir=runs", view at http://localhost:6006/')
        tb_writer = SummaryWriter(comment=opt.name)
        learner_train(hyp)

        '''
        for i in range(ways):
            learner = train(hyp)  # train normally
            print('learner_result = ',learner)
        '''

'''
    else:  # Evolve hyperparameters (optional)
        opt.notest, opt.nosave = True, True  # only test/save final epoch
        if opt.bucket:
            os.system('gsutil cp gs://%s/evolve.txt .' % opt.bucket)  # download evolve.txt if exists

        for _ in range(1):  # generations to evolve
            if os.path.exists('evolve.txt'):  # if evolve.txt exists: select best hyps and mutate
                # Select parent(s)
                parent = 'single'  # parent selection method: 'single' or 'weighted'
                x = np.loadtxt('evolve.txt', ndmin=2)
                n = min(5, len(x))  # number of previous results to consider
                x = x[np.argsort(-fitness(x))][:n]  # top n mutations
                w = fitness(x) - fitness(x).min()  # weights
                if parent == 'single' or len(x) == 1:
                    # x = x[random.randint(0, n - 1)]  # random selection
                    x = x[random.choices(range(n), weights=w)[0]]  # weighted selection
                elif parent == 'weighted':
                    x = (x * w.reshape(n, 1)).sum(0) / w.sum()  # weighted combination

                # Mutate
                method, mp, s = 3, 0.9, 0.2  # method, mutation probability, sigma
                npr = np.random
                npr.seed(int(time.time()))
                g = np.array([1, 1, 1, 1, 1, 1, 1, 0, .1, 1, 0, 1, 1, 1, 1, 1, 1, 1])  # gains
                ng = len(g)
                if method == 1:
                    v = (npr.randn(ng) * npr.random() * g * s + 1) ** 2.0
                elif method == 2:
                    v = (npr.randn(ng) * npr.random(ng) * g * s + 1) ** 2.0
                elif method == 3:
                    v = np.ones(ng)
                    while all(v == 1):  # mutate until a change occurs (prevent duplicates)
                        # v = (g * (npr.random(ng) < mp) * npr.randn(ng) * s + 1) ** 2.0
                        v = (g * (npr.random(ng) < mp) * npr.randn(ng) * npr.random() * s + 1).clip(0.3, 3.0)
                for i, k in enumerate(hyp.keys()):  # plt.hist(v.ravel(), 300)
                    hyp[k] = x[i + 7] * v[i]  # mutate

            # Clip to limits
            keys = ['lr0', 'iou_t', 'momentum', 'weight_decay', 'hsv_s', 'hsv_v', 'translate', 'scale', 'fl_gamma']
            limits = [(1e-5, 1e-2), (0.00, 0.70), (0.60, 0.98), (0, 0.001), (0, .9), (0, .9), (0, .9), (0, .9), (0, 3)]
            for k, v in zip(keys, limits):
                hyp[k] = np.clip(hyp[k], v[0], v[1])

            # Train mutation
            results = train(hyp.copy())

            # Write mutation results
            print_mutation(hyp, results, opt.bucket)

            # Plot results
            # plot_evolution_results(hyp)
'''

