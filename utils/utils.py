import glob
import math
import os
import random
import shutil
import subprocess
import time
from copy import copy
from pathlib import Path
from sys import platform

import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torchvision
from tqdm import tqdm

from . import torch_utils  # , google_utils

from scipy.stats import wasserstein_distance

# Set printoptions
torch.set_printoptions(linewidth=320, precision=5, profile='long')
np.set_printoptions(linewidth=320, formatter={'float_kind': '{:11.5g}'.format})  # format short g, %precision=5
matplotlib.rc('font', **{'size': 11})

# Prevent OpenCV from multithreading (to use PyTorch DataLoader)
cv2.setNumThreads(0)

from Module_Store import *
# WST_module_name = 'Single'
# '''
# 包含：Single, Same, None
# 说明：
#     Single: WST Head 单独存在
#     Same: WST Head 与 yolo Head 同一个
#     None: 没有 WST 功能时，原 YOLO
# '''
#
# # wst_gain for compute loss
# wst_gain_1 = 0.8
# wst_gain_2 = 0.5
# wst_loss = 'v3'
# '''
# 无所谓 WST Head 是否单独，包含：v1, v3
# 说明：
#     v1：关键点加权差值方法, 直接差值也在这里, loss_module_name = 'v1' 或 'v1_Cosdis'
#     v3：类别均值向量全局差值方法, loss_module_name = 'v3' 或 'v3_Cosdis'
# '''
# loss_module_name = 'v3'
# '''
# 当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
# 说明：
#     v1: L1, L2, Cosine of v1
#     v1_cosdis: Cosdis of v1
#     v3: L1, L2, Cosine of v3
#     v3_cosdis: Cosdis of v3
# 当 WST Head 与 yolo Head 同一时，包含：v1, v3
# 说明：
#     v1: L1, L2, Cosine of v1
#     v3: L1, L2, Cosine of v3
# '''
#
# measure = 'Cosine'
# '''
# 无所谓 WST Head 是否单独，包含：L1, L2, Cosine
# 说明：
#     当 loss_module_name = 'v1_cosdis' 或 'v3_cosdis' 时，measure随意，因为不起作用
# '''
#
# BN_WST_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做度量前 Feature maps 做 BN 操作
#     None: 在做度量前 Feature maps 不做 BN 操作
# '''
#
# OHEM_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做Loss前做 OHEM 难例硬筛选操作
#     None: 在做Loss前不做 OHEM 难例硬筛选操作
# '''
#
# multi_task_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做WST Loss前做多任务优化操作
#     None: 在做WST Loss前不做多任务优化操作
# '''


def init_seeds(seed=0):
    random.seed(seed)
    np.random.seed(seed)
    torch_utils.init_seeds(seed=seed)


def check_git_status():
    if platform in ['linux', 'darwin']:
        # Suggest 'git pull' if repo is out of date
        s = subprocess.check_output('if [ -d .git ]; then git fetch && git status -uno; fi', shell=True).decode('utf-8')
        if 'Your branch is behind' in s:
            print(s[s.find('Your branch is behind'):s.find('\n\n')] + '\n')


def check_file(file):
    # Searches for file if not found locally
    if os.path.isfile(file):
        return file
    else:
        files = glob.glob('./**/' + file, recursive=True)  # find file
        assert len(files), 'File Not Found: %s' % file  # assert file was found
        return files[0]  # return first file if multiple found


def load_classes(path):
    # Loads *.names file at 'path'
    with open(path, 'r') as f:
        names = f.read().split('\n')
    return list(filter(None, names))  # filter removes empty strings (such as last line)


def labels_to_class_weights(labels, nc=80):
    # Get class weights (inverse frequency) from training labels
    if labels[0] is None:  # no labels loaded
        return torch.Tensor()

    labels = np.concatenate(labels, 0)  # labels.shape = (866643, 5) for COCO
    classes = labels[:, 0].astype(np.int)  # labels = [class xywh]
    weights = np.bincount(classes, minlength=nc)  # occurences per class

    # Prepend gridpoint count (for uCE trianing)
    # gpi = ((320 / 32 * np.array([1, 2, 4])) ** 2 * 3).sum()  # gridpoints per image
    # weights = np.hstack([gpi * len(labels)  - weights.sum() * 9, weights * 9]) ** 0.5  # prepend gridpoints to start

    weights[weights == 0] = 1  # replace empty bins with 1
    weights = 1 / weights  # number of targets per class
    weights /= weights.sum()  # normalize
    return torch.from_numpy(weights)


def labels_to_image_weights(labels, nc=80, class_weights=np.ones(80)):
    # Produces image weights based on class mAPs
    n = len(labels)
    class_counts = np.array([np.bincount(labels[i][:, 0].astype(np.int), minlength=nc) for i in range(n)])
    image_weights = (class_weights.reshape(1, nc) * class_counts).sum(1)
    # index = random.choices(range(n), weights=image_weights, k=1)  # weight image sample
    return image_weights


def coco80_to_coco91_class():  # converts 80-index (val2014) to 91-index (paper)
    # https://tech.amikelive.com/node-718/what-object-categories-labels-are-in-coco-dataset/
    # a = np.loadtxt('data/coco.names', dtype='str', delimiter='\n')
    # b = np.loadtxt('data/coco_paper.names', dtype='str', delimiter='\n')
    # x1 = [list(a[i] == b).index(True) + 1 for i in range(80)]  # darknet to coco
    # x2 = [list(b[i] == a).index(True) if any(b[i] == a) else None for i in range(91)]  # coco to darknet
    x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34,
         35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
         64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
    return x


def xyxy2xywh(x):
    # Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2  # x center
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2  # y center
    y[:, 2] = x[:, 2] - x[:, 0]  # width
    y[:, 3] = x[:, 3] - x[:, 1]  # height
    return y


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y


def scale_coords(img1_shape, coords, img0_shape, ratio_pad=None):
    # Rescale coords (xyxy) from img1_shape to img0_shape
    if ratio_pad is None:  # calculate from img0_shape
        gain = max(img1_shape) / max(img0_shape)  # gain  = old / new
        pad = (img1_shape[1] - img0_shape[1] * gain) / 2, (img1_shape[0] - img0_shape[0] * gain) / 2  # wh padding
    else:
        gain = ratio_pad[0][0]
        pad = ratio_pad[1]

    coords[:, [0, 2]] -= pad[0]  # x padding
    coords[:, [1, 3]] -= pad[1]  # y padding
    coords[:, :4] /= gain
    clip_coords(coords, img0_shape)
    return coords


def clip_coords(boxes, img_shape):
    # Clip bounding xyxy bounding boxes to image shape (height, width)
    boxes[:, 0].clamp_(0, img_shape[1])  # x1
    boxes[:, 1].clamp_(0, img_shape[0])  # y1
    boxes[:, 2].clamp_(0, img_shape[1])  # x2
    boxes[:, 3].clamp_(0, img_shape[0])  # y2


def ap_per_class(tp, conf, pred_cls, target_cls):
    """ Compute the average precision, given the recall and precision curves.
    Source: https://github.com/rafaelpadilla/Object-Detection-Metrics.
    # Arguments
        tp:    True positives (nparray, nx1 or nx10).
        conf:  Objectness value from 0-1 (nparray).
        pred_cls: Predicted object classes (nparray).
        target_cls: True object classes (nparray).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """

    # Sort by objectness
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]

    # Find unique classes
    unique_classes = np.unique(target_cls)

    # Create Precision-Recall curve and compute AP for each class
    pr_score = 0.1  # score to evaluate P and R https://github.com/ultralytics/yolov3/issues/898
    s = [unique_classes.shape[0], tp.shape[1]]  # number class, number iou thresholds (i.e. 10 for mAP0.5...0.95)
    ap, p, r = np.zeros(s), np.zeros(s), np.zeros(s)
    for ci, c in enumerate(unique_classes):
        i = pred_cls == c
        n_gt = (target_cls == c).sum()  # Number of ground truth objects
        n_p = i.sum()  # Number of predicted objects

        if n_p == 0 or n_gt == 0:
            continue
        else:
            # Accumulate FPs and TPs
            fpc = (1 - tp[i]).cumsum(0)
            tpc = tp[i].cumsum(0)

            # Recall
            recall = tpc / (n_gt + 1e-16)  # recall curve
            r[ci] = np.interp(-pr_score, -conf[i], recall[:, 0])  # r at pr_score, negative x, xp because xp decreases

            # Precision
            precision = tpc / (tpc + fpc)  # precision curve
            p[ci] = np.interp(-pr_score, -conf[i], precision[:, 0])  # p at pr_score

            # AP from recall-precision curve
            for j in range(tp.shape[1]):
                ap[ci, j] = compute_ap(recall[:, j], precision[:, j])

            # Plot
            # fig, ax = plt.subplots(1, 1, figsize=(5, 5))
            # ax.plot(recall, precision)
            # ax.set_xlabel('Recall')
            # ax.set_ylabel('Precision')
            # ax.set_xlim(0, 1.01)
            # ax.set_ylim(0, 1.01)
            # fig.tight_layout()
            # fig.savefig('PR_curve.png', dpi=300)

    # Compute F1 score (harmonic mean of precision and recall)
    f1 = 2 * p * r / (p + r + 1e-16)

    return p, r, ap, f1, unique_classes.astype('int32')


def compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves.
    Source: https://github.com/rbgirshick/py-faster-rcnn.
    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """

    # Append sentinel values to beginning and end
    mrec = np.concatenate(([0.], recall, [min(recall[-1] + 1E-3, 1.)]))
    mpre = np.concatenate(([0.], precision, [0.]))

    # Compute the precision envelope
    mpre = np.flip(np.maximum.accumulate(np.flip(mpre)))

    # Integrate area under curve
    method = 'interp'  # methods: 'continuous', 'interp'
    if method == 'interp':
        x = np.linspace(0, 1, 101)  # 101-point interp (COCO)
        ap = np.trapz(np.interp(x, mrec, mpre), x)  # integrate
    else:  # 'continuous'
        i = np.where(mrec[1:] != mrec[:-1])[0]  # points where x axis (recall) changes
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])  # area under curve

    return ap


def bbox_iou(box1, box2, x1y1x2y2=True, GIoU=False, DIoU=False, CIoU=False):
    # Returns the IoU of box1 to box2. box1 is 4, box2 is nx4
    box2 = box2.t()

    # Get the coordinates of bounding boxes
    if x1y1x2y2:  # x1, y1, x2, y2 = box1
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[0], box1[1], box1[2], box1[3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[0], box2[1], box2[2], box2[3]
    else:  # transform from xywh to xyxy
        b1_x1, b1_x2 = box1[0] - box1[2] / 2, box1[0] + box1[2] / 2
        b1_y1, b1_y2 = box1[1] - box1[3] / 2, box1[1] + box1[3] / 2
        b2_x1, b2_x2 = box2[0] - box2[2] / 2, box2[0] + box2[2] / 2
        b2_y1, b2_y2 = box2[1] - box2[3] / 2, box2[1] + box2[3] / 2

    # Intersection area
    inter = (torch.min(b1_x2, b2_x2) - torch.max(b1_x1, b2_x1)).clamp(0) * \
            (torch.min(b1_y2, b2_y2) - torch.max(b1_y1, b2_y1)).clamp(0)

    # Union Area
    w1, h1 = b1_x2 - b1_x1, b1_y2 - b1_y1
    w2, h2 = b2_x2 - b2_x1, b2_y2 - b2_y1
    union = (w1 * h1 + 1e-16) + w2 * h2 - inter

    iou = inter / union  # iou
    if GIoU or DIoU or CIoU:
        cw = torch.max(b1_x2, b2_x2) - torch.min(b1_x1, b2_x1)  # convex (smallest enclosing box) width
        ch = torch.max(b1_y2, b2_y2) - torch.min(b1_y1, b2_y1)  # convex height
        if GIoU:  # Generalized IoU https://arxiv.org/pdf/1902.09630.pdf
            c_area = cw * ch + 1e-16  # convex area
            return iou - (c_area - union) / c_area  # GIoU
        if DIoU or CIoU:  # Distance or Complete IoU https://arxiv.org/abs/1911.08287v1
            # convex diagonal squared
            c2 = cw ** 2 + ch ** 2 + 1e-16
            # centerpoint distance squared
            rho2 = ((b2_x1 + b2_x2) - (b1_x1 + b1_x2)) ** 2 / 4 + ((b2_y1 + b2_y2) - (b1_y1 + b1_y2)) ** 2 / 4
            if DIoU:
                return iou - rho2 / c2  # DIoU
            elif CIoU:  # https://github.com/Zzh-tju/DIoU-SSD-pytorch/blob/master/utils/box/box_utils.py#L47
                v = (4 / math.pi ** 2) * torch.pow(torch.atan(w2 / h2) - torch.atan(w1 / h1), 2)
                with torch.no_grad():
                    alpha = v / (1 - iou + v)
                return iou - (rho2 / c2 + v * alpha)  # CIoU

    return iou


def box_iou(box1, box2):
    # https://github.com/pytorch/vision/blob/master/torchvision/ops/boxes.py
    """
    Return intersection-over-union (Jaccard index) of boxes.
    Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
    Arguments:
        box1 (Tensor[N, 4])
        box2 (Tensor[M, 4])
    Returns:
        iou (Tensor[N, M]): the NxM matrix containing the pairwise
            IoU values for every element in boxes1 and boxes2
    """

    def box_area(box):
        # box = 4xn
        return (box[2] - box[0]) * (box[3] - box[1])

    area1 = box_area(box1.t())
    area2 = box_area(box2.t())

    # inter(N,M) = (rb(N,M,2) - lt(N,M,2)).clamp(0).prod(2)
    inter = (torch.min(box1[:, None, 2:], box2[:, 2:]) - torch.max(box1[:, None, :2], box2[:, :2])).clamp(0).prod(2)
    return inter / (area1[:, None] + area2 - inter)  # iou = inter / (area1 + area2 - inter)


def wh_iou(wh1, wh2):
    # Returns the nxm IoU matrix. wh1 is nx2, wh2 is mx2
    wh1 = wh1[:, None]  # [N,1,2]
    wh2 = wh2[None]  # [1,M,2]
    inter = torch.min(wh1, wh2).prod(2)  # [N,M]
    return inter / (wh1.prod(2) + wh2.prod(2) - inter)  # iou = inter / (area1 + area2 - inter)


class FocalLoss(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, loss_fcn, gamma=1.5, alpha=0.25):
        super(FocalLoss, self).__init__()
        self.loss_fcn = loss_fcn  # must be nn.BCEWithLogitsLoss()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = loss_fcn.reduction
        self.loss_fcn.reduction = 'none'  # required to apply FL to each element

    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)
        # p_t = torch.exp(-loss)
        # loss *= self.alpha * (1.000001 - p_t) ** self.gamma  # non-zero power for gradient stability

        # TF implementation https://github.com/tensorflow/addons/blob/v0.7.1/tensorflow_addons/losses/focal_loss.py
        pred_prob = torch.sigmoid(pred)  # prob from logits
        p_t = true * pred_prob + (1 - true) * (1 - pred_prob)
        alpha_factor = true * self.alpha + (1 - true) * (1 - self.alpha)
        modulating_factor = (1.0 - p_t) ** self.gamma
        loss *= alpha_factor * modulating_factor

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:  # 'none'
            return loss

class RoyFocalLoss(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, gamma=1.5, alpha=0.25, reduction='elementwise_mean'):
        super(RoyFocalLoss, self).__init__()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = reduction

    def forward(self, pred, true):
        '''========================== Meta Learning ================================='''
        # pred_prob = nn.functional.tanh(pred)  # prob from logits
        # pred_prob = torch.tanh(pred)  # prob from logits

        # #softmax
        # pred_sum = pred.sum()
        # pred_prob = pred.clone()
        # pred_prob /= pred_sum

        pred_sum = pred.sum()
        # print('pred_sum = \n', pred_sum)
        # pred_max = torch.max(pred)
        pred_prob = pred.clone()
        # pred_prob -= pred_max * 0.5
        pred_prob = torch.sigmoid(pred_prob)

        # print('pred = %s \n pred_prob = %s \n' % (pred, pred_prob))

        # loss = - (1 - pred_prob) ** self.gamma * true * self.alpha * torch.log(pred_prob) \
        #        - pred_prob ** self.gamma * (1 - true) * (1 - self.alpha) * torch.log(1 - pred_prob)

        loss_y1 = - (1.5 - pred_prob) ** self.gamma * true * self.alpha * torch.log(pred_prob - 0.5)
        loss_y0 = - (pred_prob - 0.5) ** self.gamma * (1 - true) * (1 - self.alpha) * torch.log(1.5 - pred_prob)
        loss = loss_y0 + loss_y1

        nl = loss.shape[0]
        find_nan = torch.isnan(loss)
        # find_inf = torch.isfinite(loss)
        for i in range(nl):
            for j in range(i, nl):
                if find_nan[i, j]:
                    loss[i, j] = 0.0
                    loss[j, i] = 0.0

                if true[i, j] == 1.0 and pred_prob[i, j] == 0.5:
                    loss[i, j] = pred_sum
                    loss[j, i] = pred_sum

                if true[i, j] == 0.0 and pred_prob[i, j] == 1.0:
                    loss[i, j] = pred_sum
                    loss[j, i] = pred_sum

        # print('loss_y1 = %s \n loss_y0 = %s \n loss = %s \n' % (loss_y1, loss_y0, loss))

        if self.reduction == 'elementwise_mean':
            return loss.mean(), pred_sum
        elif self.reduction == 'sum':
            return loss.sum(), pred_sum
        else:  # 'none'
            return loss, pred_sum

def smooth_BCE(eps=0.1):  # https://github.com/ultralytics/yolov3/issues/238#issuecomment-598028441
    # return positive, negative label smoothing BCE targets
    return 1.0 - 0.5 * eps, 0.5 * eps


def compute_loss(p, targets, model):  # predictions, targets, model
    ft = torch.cuda.FloatTensor if p[0].is_cuda else torch.Tensor
    lcls, lbox, lobj = ft([0]), ft([0]), ft([0])
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    h = model.hyp  # hyperparameters
    red = 'mean'  # Loss reduction (sum or mean)

    # Define criteria
    BCEcls = nn.BCEWithLogitsLoss(pos_weight=ft([h['cls_pw']]), reduction=red)
    BCEobj = nn.BCEWithLogitsLoss(pos_weight=ft([h['obj_pw']]), reduction=red)

    # class label smoothing https://arxiv.org/pdf/1902.04103.pdf eqn 3
    cp, cn = smooth_BCE(eps=0.0)

    # focal loss
    g = h['fl_gamma']  # focal loss gamma
    if g > 0:
        BCEcls, BCEobj = FocalLoss(BCEcls, g), FocalLoss(BCEobj, g)

    '''========================== Meta Learning ================================='''
    lwst = ft([0])
    BCEwst = RoyFocalLoss()

    # per output
    nt = 0  # targets
    gwst = 0.0
    for i, pi in enumerate(p):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))

        tobj = torch.zeros_like(pi[..., 0])  # target obj  # tobj用于存放置信度
        # print("pi[..., 0] ===>", pi[..., 0])

        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            '''========================== Meta Learning =================================
            ps是能匹配上target的基于anchor box的feature vec
            可以利用ps做元学习的对比学习设计
            需要设计度量，设计loss
            '''
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # GIoU
            pxy = ps[:, :2].sigmoid()
            pwh = ps[:, 2:4].exp().clamp(max=1E3) * anchors[i]
            pbox = torch.cat((pxy, pwh), 1)  # predicted box
            giou = bbox_iou(pbox.t(), tbox[i], x1y1x2y2=False, GIoU=True)  # giou(prediction, target)
            lbox += (1.0 - giou).sum() if red == 'sum' else (1.0 - giou).mean()  # giou loss

            # Obj
            tobj[b, a, gj, gi] = (1.0 - model.gr) + model.gr * giou.detach().clamp(0).type(tobj.dtype)  # giou ratio

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp
                lcls += BCEcls(ps[:, 5:], t)  # BCE

                '''========================== Meta Learning ================================='''
                if nb > 1:
                    wst_d = np.zeros(shape=(nb, nb), dtype=float)
                    wst_d_ps = ps.clone()
                    wst_d_ps = wst_d_ps.cpu().detach().numpy()

                    wst_t = np.zeros(shape=(nb, nb), dtype=float)
                    wst_d_t = t.clone()
                    wst_d_t = wst_d_t.cpu().detach().numpy()
                    for wst_di in range(nb):
                        for wst_dj in range(wst_di + 1, nb):
                            wst_d[wst_di, wst_dj] = wasserstein_distance(wst_d_ps[wst_di, 5:], wst_d_ps[wst_dj, 5:])
                            wst_d[wst_dj, wst_di] = wst_d[wst_di, wst_dj]
                            wst_t[wst_di, wst_dj] = 0.0 if (wst_d_t[wst_di] == wst_d_t[wst_dj]).all() else 1.0
                            wst_t[wst_dj, wst_di] = wst_t[wst_di, wst_dj]

                    wst_pre = torch.tensor(wst_d, requires_grad=True)
                    # wst_pre = torch.tensor(wst_d)
                    wst_pre = wst_pre.to(targets.device)
                    # print('wst_pre = \n', wst_pre)
                    # wst_target = torch.tensor(wst_t, requires_grad=True)   #同类为0, 不同类为1
                    wst_target = torch.tensor(wst_t)
                    wst_target = wst_target.to(targets.device)
                    # print('wst_target = \n', wst_target)
                    loss_wst, gain_wst = BCEwst(wst_pre, wst_target)
                    gwst += gain_wst
                    # print("gwst = \n", gwst)
                    # print("gain_wst = \n", gain_wst)
                    lwst += loss_wst * gain_wst
                    # print("lwst = \n", lwst)
        ''''''

            # Append targets to text file
            # with open('targets.txt', 'a') as file:
            #     [file.write('%11.5g ' * 4 % tuple(x) + '\n') for x in torch.cat((txy[i], twh[i]), 1)]

        lobj += BCEobj(pi[..., 4], tobj)  # obj loss
        # print("lobj = ", lobj)

    lbox *= h['giou']
    lobj *= h['obj']
    lcls *= h['cls']

    '''========================== Meta Learning ================================='''
    # print('nt = ', nt)
    if nt > 1:
        if gwst > 0:
            # print("什么情况gwst = \n", gwst)
            lwst *= h['wst'] * h['wst_pw'] / gwst
            # print('lwst = ', lwst)

    if red == 'sum':
        bs = tobj.shape[0]  # batch size
        g = 3.0  # loss gain
        lobj *= g / bs
        if nt:
            lcls *= g / nt / model.nc
            lbox *= g / nt
            lwst *= g / nt / nt / 2

    # loss = lbox + lobj + lcls
    '''========================== Meta Learning ================================='''
    loss = lbox + lobj + lcls + lwst
    # print("lbox = %s \n lobj = %s \n lcls = %s \n lwst = %s" % (lbox, lobj, lcls, lwst))
    return loss, torch.cat((lbox, lobj, lcls, lwst, loss)).detach()

    # return loss, torch.cat((lbox, lobj, lcls, loss)).detach()


def build_targets(p, targets, model):
    # Build targets for compute_loss(), input targets(image,class,x,y,w,h)
    nt = targets.shape[0]
    tcls, tbox, indices, anch = [], [], [], []
    gain = torch.ones(6, device=targets.device)  # normalized to gridspace gain
    off = torch.tensor([[1, 0], [0, 1], [-1, 0], [0, -1]], device=targets.device).float()  # overlap offsets

    style = None
    multi_gpu = type(model) in (nn.parallel.DataParallel, nn.parallel.DistributedDataParallel)
    for i, j in enumerate(model.yolo_layers):
        anchors = model.module.module_list[j].anchor_vec if multi_gpu else model.module_list[j].anchor_vec
        gain[2:] = torch.tensor(p[i].shape)[[3, 2, 3, 2]]  # xyxy gain

        na = anchors.shape[0]  # number of anchors
        at = torch.arange(na).view(na, 1).repeat(1, nt)  # anchor tensor, same as .repeat_interleave(nt)

        # Match targets to anchors
        a, t, offsets = [], targets * gain, 0

        if nt:
            # r = t[None, :, 4:6] / anchors[:, None]  # wh ratio
            # j = torch.max(r, 1. / r).max(2)[0] < model.hyp['anchor_t']  # compare
            j = wh_iou(anchors, t[:, 4:6]) > model.hyp['iou_t']  # iou(3,n) = wh_iou(anchors(3,2), gwh(n,2))
            a, t = at[j], t.repeat(na, 1, 1)[j]  # filter

            # overlaps
            gxy = t[:, 2:4]  # grid xy
            z = torch.zeros_like(gxy)
            if style == 'rect2':
                g = 0.2  # offset
                j, k = ((gxy % 1. < g) & (gxy > 1.)).T
                a, t = torch.cat((a, a[j], a[k]), 0), torch.cat((t, t[j], t[k]), 0)
                offsets = torch.cat((z, z[j] + off[0], z[k] + off[1]), 0) * g

            elif style == 'rect4':
                g = 0.5  # offset
                j, k = ((gxy % 1. < g) & (gxy > 1.)).T
                l, m = ((gxy % 1. > (1 - g)) & (gxy < (gain[[2, 3]] - 1.))).T
                a, t = torch.cat((a, a[j], a[k], a[l], a[m]), 0), torch.cat((t, t[j], t[k], t[l], t[m]), 0)
                offsets = torch.cat((z, z[j] + off[0], z[k] + off[1], z[l] + off[2], z[m] + off[3]), 0) * g

        # Define
        b, c = t[:, :2].long().T  # image, class
        gxy = t[:, 2:4]  # grid xy
        gwh = t[:, 4:6]  # grid wh
        gij = (gxy - offsets).long()
        gi, gj = gij.T  # grid xy indices

        # Append
        indices.append((b, a, gj, gi))  # image, anchor, grid indices
        tbox.append(torch.cat((gxy - gij, gwh), 1))  # box
        anch.append(anchors[a])  # anchors
        tcls.append(c)  # class
        if c.shape[0]:  # if any targets
            assert c.max() < model.nc, 'Model accepts %g classes labeled from 0-%g, however you labelled a class %g. ' \
                                       'See https://github.com/ultralytics/yolov3/wiki/Train-Custom-Data' % (
                                           model.nc, model.nc - 1, c.max())

    return tcls, tbox, indices, anch


def non_max_suppression(prediction, conf_thres=0.1, iou_thres=0.6, multi_label=True, classes=None, agnostic=False):
    """
    Performs  Non-Maximum Suppression on inference results
    Returns detections with shape:
        nx6 (x1, y1, x2, y2, conf, cls)
    """

    # Settings
    merge = True  # merge for best mAP
    min_wh, max_wh = 2, 4096  # (pixels) minimum and maximum box width and height
    time_limit = 10.0  # seconds to quit after

    t = time.time()
    nc = prediction[0].shape[1] - 5  # number of classes
    multi_label &= nc > 1  # multiple labels per box
    output = [None] * prediction.shape[0]
    for xi, x in enumerate(prediction):  # image index, image inference
        # Apply constraints
        x = x[x[:, 4] > conf_thres]  # confidence
        x = x[((x[:, 2:4] > min_wh) & (x[:, 2:4] < max_wh)).all(1)]  # width-height

        # If none remain process next image
        if not x.shape[0]:
            continue

        # Compute conf
        x[..., 5:] *= x[..., 4:5]  # conf = obj_conf * cls_conf

        # Box (center x, center y, width, height) to (x1, y1, x2, y2)
        box = xywh2xyxy(x[:, :4])

        # Detections matrix nx6 (xyxy, conf, cls)
        if multi_label:
            i, j = (x[:, 5:] > conf_thres).nonzero().t()
            x = torch.cat((box[i], x[i, j + 5].unsqueeze(1), j.float().unsqueeze(1)), 1)
        else:  # best class only
            conf, j = x[:, 5:].max(1)
            x = torch.cat((box, conf.unsqueeze(1), j.float().unsqueeze(1)), 1)[conf > conf_thres]

        # Filter by class
        if classes:
            x = x[(j.view(-1, 1) == torch.tensor(classes, device=j.device)).any(1)]

        # Apply finite constraint
        # if not torch.isfinite(x).all():
        #     x = x[torch.isfinite(x).all(1)]

        # If none remain process next image
        n = x.shape[0]  # number of boxes
        if not n:
            continue

        # Sort by confidence
        # x = x[x[:, 4].argsort(descending=True)]

        # Batched NMS
        c = x[:, 5] * 0 if agnostic else x[:, 5]  # classes
        boxes, scores = x[:, :4].clone() + c.view(-1, 1) * max_wh, x[:, 4]  # boxes (offset by class), scores
        i = torchvision.ops.boxes.nms(boxes, scores, iou_thres)
        if merge and (1 < n < 3E3):  # Merge NMS (boxes merged using weighted mean)
            try:  # update boxes as boxes(i,4) = weights(i,n) * boxes(n,4)
                iou = box_iou(boxes[i], boxes) > iou_thres  # iou matrix
                weights = iou * scores[None]  # box weights
                x[i, :4] = torch.mm(weights, x[:, :4]).float() / weights.sum(1, keepdim=True)  # merged boxes
                # i = i[iou.sum(1) > 1]  # require redundancy
            except:  # possible CUDA error https://github.com/ultralytics/yolov3/issues/1139
                print(x, i, x.shape, i.shape)
                pass

        output[xi] = x[i]
        if (time.time() - t) > time_limit:
            break  # time limit exceeded

    return output


def get_yolo_layers(model):
    bool_vec = [x['type'] == 'yolo' for x in model.module_defs]
    return [i for i, x in enumerate(bool_vec) if x]  # [82, 94, 106] for yolov3


def print_model_biases(model):
    # prints the bias neurons preceding each yolo layer
    print('\nModel Bias Summary: %8s%18s%18s%18s' % ('layer', 'regression', 'objectness', 'classification'))
    try:
        multi_gpu = type(model) in (nn.parallel.DataParallel, nn.parallel.DistributedDataParallel)
        for l in model.yolo_layers:  # print pretrained biases
            if multi_gpu:
                na = model.module.module_list[l].na  # number of anchors
                b = model.module.module_list[l - 1][0].bias.view(na, -1)  # bias 3x85
            else:
                na = model.module_list[l].na
                b = model.module_list[l - 1][0].bias.view(na, -1)  # bias 3x85
            print(' ' * 20 + '%8g %18s%18s%18s' % (l, '%5.2f+/-%-5.2f' % (b[:, :4].mean(), b[:, :4].std()),
                                                   '%5.2f+/-%-5.2f' % (b[:, 4].mean(), b[:, 4].std()),
                                                   '%5.2f+/-%-5.2f' % (b[:, 5:].mean(), b[:, 5:].std())))
    except:
        pass


def strip_optimizer(f='weights/best.pt'):  # from utils.utils import *; strip_optimizer()
    # Strip optimizer from *.pt files for lighter files (reduced by 2/3 size)
    x = torch.load(f, map_location=torch.device('cpu'))
    x['optimizer'] = None
    print('Optimizer stripped from %s' % f)
    torch.save(x, f)


def create_backbone(f='weights/best.pt'):  # from utils.utils import *; create_backbone()
    # create a backbone from a *.pt file
    x = torch.load(f, map_location=torch.device('cpu'))
    x['optimizer'] = None
    x['training_results'] = None
    x['epoch'] = -1
    for p in x['model'].parameters():
        p.requires_grad = True
    s = 'weights/backbone.pt'
    print('%s saved as %s' % (f, s))
    torch.save(x, s)


def coco_class_count(path='../coco/labels/train2014/'):
    # Histogram of occurrences per class
    nc = 80  # number classes
    x = np.zeros(nc, dtype='int32')
    files = sorted(glob.glob('%s/*.*' % path))
    for i, file in enumerate(files):
        labels = np.loadtxt(file, dtype=np.float32).reshape(-1, 5)
        x += np.bincount(labels[:, 0].astype('int32'), minlength=nc)
        print(i, len(files))


def coco_only_people(path='../coco/labels/train2017/'):  # from utils.utils import *; coco_only_people()
    # Find images with only people
    files = sorted(glob.glob('%s/*.*' % path))
    for i, file in enumerate(files):
        labels = np.loadtxt(file, dtype=np.float32).reshape(-1, 5)
        if all(labels[:, 0] == 0):
            print(labels.shape[0], file)


def crop_images_random(path='../images/', scale=0.50):  # from utils.utils import *; crop_images_random()
    # crops images into random squares up to scale fraction
    # WARNING: overwrites images!
    for file in tqdm(sorted(glob.glob('%s/*.*' % path))):
        img = cv2.imread(file)  # BGR
        if img is not None:
            h, w = img.shape[:2]

            # create random mask
            a = 30  # minimum size (pixels)
            mask_h = random.randint(a, int(max(a, h * scale)))  # mask height
            mask_w = mask_h  # mask width

            # box
            xmin = max(0, random.randint(0, w) - mask_w // 2)
            ymin = max(0, random.randint(0, h) - mask_h // 2)
            xmax = min(w, xmin + mask_w)
            ymax = min(h, ymin + mask_h)

            # apply random color mask
            cv2.imwrite(file, img[ymin:ymax, xmin:xmax])


def coco_single_class_labels(path='../coco/labels/train2014/', label_class=43):
    # Makes single-class coco datasets. from utils.utils import *; coco_single_class_labels()
    if os.path.exists('new/'):
        shutil.rmtree('new/')  # delete output folder
    os.makedirs('new/')  # make new output folder
    os.makedirs('new/labels/')
    os.makedirs('new/images/')
    for file in tqdm(sorted(glob.glob('%s/*.*' % path))):
        with open(file, 'r') as f:
            labels = np.array([x.split() for x in f.read().splitlines()], dtype=np.float32)
        i = labels[:, 0] == label_class
        if any(i):
            img_file = file.replace('labels', 'images').replace('txt', 'jpg')
            labels[:, 0] = 0  # reset class to 0
            with open('new/images.txt', 'a') as f:  # add image to dataset list
                f.write(img_file + '\n')
            with open('new/labels/' + Path(file).name, 'a') as f:  # write label
                for l in labels[i]:
                    f.write('%g %.6f %.6f %.6f %.6f\n' % tuple(l))
            shutil.copyfile(src=img_file, dst='new/images/' + Path(file).name.replace('txt', 'jpg'))  # copy images


def kmean_anchors(path='./data/coco64.txt', n=9, img_size=(640, 640), thr=0.20, gen=1000):
    # Creates kmeans anchors for use in *.cfg files: from utils.utils import *; _ = kmean_anchors()
    # n: number of anchors
    # img_size: (min, max) image size used for multi-scale training (can be same values)
    # thr: IoU threshold hyperparameter used for training (0.0 - 1.0)
    # gen: generations to evolve anchors using genetic algorithm
    from utils.datasets import LoadImagesAndLabels

    def print_results(k):
        k = k[np.argsort(k.prod(1))]  # sort small to large
        iou = wh_iou(wh, torch.Tensor(k))
        max_iou = iou.max(1)[0]
        bpr, aat = (max_iou > thr).float().mean(), (iou > thr).float().mean() * n  # best possible recall, anch > thr
        print('%.2f iou_thr: %.3f best possible recall, %.2f anchors > thr' % (thr, bpr, aat))
        print('n=%g, img_size=%s, IoU_all=%.3f/%.3f-mean/best, IoU>thr=%.3f-mean: ' %
              (n, img_size, iou.mean(), max_iou.mean(), iou[iou > thr].mean()), end='')
        for i, x in enumerate(k):
            print('%i,%i' % (round(x[0]), round(x[1])), end=',  ' if i < len(k) - 1 else '\n')  # use in *.cfg
        return k

    def fitness(k):  # mutation fitness
        iou = wh_iou(wh, torch.Tensor(k))  # iou
        max_iou = iou.max(1)[0]
        return (max_iou * (max_iou > thr).float()).mean()  # product

    # Get label wh
    wh = []
    dataset = LoadImagesAndLabels(path, augment=True, rect=True)
    nr = 1 if img_size[0] == img_size[1] else 10  # number augmentation repetitions
    for s, l in zip(dataset.shapes, dataset.labels):
        wh.append(l[:, 3:5] * (s / s.max()))  # image normalized to letterbox normalized wh
    wh = np.concatenate(wh, 0).repeat(nr, axis=0)  # augment 10x
    wh *= np.random.uniform(img_size[0], img_size[1], size=(wh.shape[0], 1))  # normalized to pixels (multi-scale)
    wh = wh[(wh > 2.0).all(1)]  # remove below threshold boxes (< 2 pixels wh)

    # Kmeans calculation
    from scipy.cluster.vq import kmeans
    print('Running kmeans for %g anchors on %g points...' % (n, len(wh)))
    s = wh.std(0)  # sigmas for whitening
    k, dist = kmeans(wh / s, n, iter=30)  # points, mean distance
    k *= s
    wh = torch.Tensor(wh)
    k = print_results(k)

    # # Plot
    # k, d = [None] * 20, [None] * 20
    # for i in tqdm(range(1, 21)):
    #     k[i-1], d[i-1] = kmeans(wh / s, i)  # points, mean distance
    # fig, ax = plt.subplots(1, 2, figsize=(14, 7))
    # ax = ax.ravel()
    # ax[0].plot(np.arange(1, 21), np.array(d) ** 2, marker='.')
    # fig, ax = plt.subplots(1, 2, figsize=(14, 7))  # plot wh
    # ax[0].hist(wh[wh[:, 0]<100, 0],400)
    # ax[1].hist(wh[wh[:, 1]<100, 1],400)
    # fig.tight_layout()
    # fig.savefig('wh.png', dpi=200)

    # Evolve
    npr = np.random
    f, sh, mp, s = fitness(k), k.shape, 0.9, 0.1  # fitness, generations, mutation prob, sigma
    for _ in tqdm(range(gen), desc='Evolving anchors'):
        v = np.ones(sh)
        while (v == 1).all():  # mutate until a change occurs (prevent duplicates)
            v = ((npr.random(sh) < mp) * npr.random() * npr.randn(*sh) * s + 1).clip(0.3, 3.0)
        kg = (k.copy() * v).clip(min=2.0)
        fg = fitness(kg)
        if fg > f:
            f, k = fg, kg.copy()
            print_results(k)
    k = print_results(k)

    return k


def print_mutation(hyp, results, bucket=''):
    # Print mutation results to evolve.txt (for use with train.py --evolve)
    a = '%10s' * len(hyp) % tuple(hyp.keys())  # hyperparam keys
    b = '%10.3g' * len(hyp) % tuple(hyp.values())  # hyperparam values
    c = '%10.4g' * len(results) % results  # results (P, R, mAP, F1, test_loss)
    print('\n%s\n%s\nEvolved fitness: %s\n' % (a, b, c))

    if bucket:
        os.system('gsutil cp gs://%s/evolve.txt .' % bucket)  # download evolve.txt

    with open('evolve.txt', 'a') as f:  # append result
        f.write(c + b + '\n')
    x = np.unique(np.loadtxt('evolve.txt', ndmin=2), axis=0)  # load unique rows
    np.savetxt('evolve.txt', x[np.argsort(-fitness(x))], '%10.3g')  # save sort by fitness

    if bucket:
        os.system('gsutil cp evolve.txt gs://%s' % bucket)  # upload evolve.txt


def apply_classifier(x, model, img, im0):
    # applies a second stage classifier to yolo outputs
    im0 = [im0] if isinstance(im0, np.ndarray) else im0
    for i, d in enumerate(x):  # per image
        if d is not None and len(d):
            d = d.clone()

            # Reshape and pad cutouts
            b = xyxy2xywh(d[:, :4])  # boxes
            b[:, 2:] = b[:, 2:].max(1)[0].unsqueeze(1)  # rectangle to square
            b[:, 2:] = b[:, 2:] * 1.3 + 30  # pad
            d[:, :4] = xywh2xyxy(b).long()

            # Rescale boxes from img_size to im0 size
            scale_coords(img.shape[2:], d[:, :4], im0[i].shape)

            # Classes
            pred_cls1 = d[:, 5].long()
            ims = []
            for j, a in enumerate(d):  # per item
                cutout = im0[i][int(a[1]):int(a[3]), int(a[0]):int(a[2])]
                im = cv2.resize(cutout, (224, 224))  # BGR
                # cv2.imwrite('test%i.jpg' % j, cutout)

                im = im[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
                im = np.ascontiguousarray(im, dtype=np.float32)  # uint8 to float32
                im /= 255.0  # 0 - 255 to 0.0 - 1.0
                ims.append(im)

            pred_cls2 = model(torch.Tensor(ims).to(d.device)).argmax(1)  # classifier prediction
            x[i] = x[i][pred_cls1 == pred_cls2]  # retain matching class detections

    return x


def fitness(x):
    # Returns fitness (for use with results.txt or evolve.txt)
    w = [0.0, 0.01, 0.99, 0.00]  # weights for [P, R, mAP, F1]@0.5 or [P, R, mAP@0.5, mAP@0.5:0.95]
    return (x[:, :4] * w).sum(1)


def output_to_target(output, width, height):
    """
    Convert a YOLO model output to target format
    [batch_id, class_id, x, y, w, h, conf]
    """
    if isinstance(output, torch.Tensor):
        output = output.cpu().numpy()

    targets = []
    for i, o in enumerate(output):
        if o is not None:
            for pred in o:
                box = pred[:4]
                w = (box[2] - box[0]) / width
                h = (box[3] - box[1]) / height
                x = box[0] / width + w / 2
                y = box[1] / height + h / 2
                conf = pred[4]
                cls = int(pred[5])

                targets.append([i, cls, x, y, w, h, conf])

    return np.array(targets)


# Plotting functions ---------------------------------------------------------------------------------------------------
def plot_one_box(x, img, color=None, label=None, line_thickness=None):
    # Plots one bounding box on image img
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)


def plot_wh_methods():  # from utils.utils import *; plot_wh_methods()
    # Compares the two methods for width-height anchor multiplication
    # https://github.com/ultralytics/yolov3/issues/168
    x = np.arange(-4.0, 4.0, .1)
    ya = np.exp(x)
    yb = torch.sigmoid(torch.from_numpy(x)).numpy() * 2

    fig = plt.figure(figsize=(6, 3), dpi=150)
    plt.plot(x, ya, '.-', label='yolo method')
    plt.plot(x, yb ** 2, '.-', label='^2 power method')
    plt.plot(x, yb ** 2.5, '.-', label='^2.5 power method')
    plt.xlim(left=-4, right=4)
    plt.ylim(bottom=0, top=6)
    plt.xlabel('input')
    plt.ylabel('output')
    plt.legend()
    fig.tight_layout()
    fig.savefig('comparison.png', dpi=200)


def plot_images(images, targets, paths=None, fname='images.jpg', names=None, max_size=640, max_subplots=16):
    tl = 3  # line thickness
    tf = max(tl - 1, 1)  # font thickness
    if os.path.isfile(fname):  # do not overwrite
        return None

    if isinstance(images, torch.Tensor):
        images = images.cpu().numpy()

    if isinstance(targets, torch.Tensor):
        targets = targets.cpu().numpy()

    # un-normalise
    if np.max(images[0]) <= 1:
        images *= 255

    bs, _, h, w = images.shape  # batch size, _, height, width
    bs = min(bs, max_subplots)  # limit plot images
    ns = np.ceil(bs ** 0.5)  # number of subplots (square)

    # Check if we should resize
    scale_factor = max_size / max(h, w)
    if scale_factor < 1:
        h = math.ceil(scale_factor * h)
        w = math.ceil(scale_factor * w)

    # Empty array for output
    mosaic = np.full((int(ns * h), int(ns * w), 3), 255, dtype=np.uint8)

    # Fix class - colour map
    prop_cycle = plt.rcParams['axes.prop_cycle']
    # https://stackoverflow.com/questions/51350872/python-from-color-name-to-rgb
    hex2rgb = lambda h: tuple(int(h[1 + i:1 + i + 2], 16) for i in (0, 2, 4))
    color_lut = [hex2rgb(h) for h in prop_cycle.by_key()['color']]

    for i, img in enumerate(images):
        if i == max_subplots:  # if last batch has fewer images than we expect
            break

        block_x = int(w * (i // ns))
        block_y = int(h * (i % ns))

        img = img.transpose(1, 2, 0)
        if scale_factor < 1:
            img = cv2.resize(img, (w, h))

        mosaic[block_y:block_y + h, block_x:block_x + w, :] = img
        if len(targets) > 0:
            image_targets = targets[targets[:, 0] == i]
            boxes = xywh2xyxy(image_targets[:, 2:6]).T
            classes = image_targets[:, 1].astype('int')
            gt = image_targets.shape[1] == 6  # ground truth if no conf column
            conf = None if gt else image_targets[:, 6]  # check for confidence presence (gt vs pred)

            boxes[[0, 2]] *= w
            boxes[[0, 2]] += block_x
            boxes[[1, 3]] *= h
            boxes[[1, 3]] += block_y
            for j, box in enumerate(boxes.T):
                cls = int(classes[j])
                color = color_lut[cls % len(color_lut)]
                cls = names[cls] if names else cls
                if gt or conf[j] > 0.3:  # 0.3 conf thresh
                    label = '%s' % cls if gt else '%s %.1f' % (cls, conf[j])
                    plot_one_box(box, mosaic, label=label, color=color, line_thickness=tl)

        # Draw image filename labels
        if paths is not None:
            label = os.path.basename(paths[i])[:40]  # trim to 40 char
            t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
            cv2.putText(mosaic, label, (block_x + 5, block_y + t_size[1] + 5), 0, tl / 3, [220, 220, 220], thickness=tf,
                        lineType=cv2.LINE_AA)

        # Image border
        cv2.rectangle(mosaic, (block_x, block_y), (block_x + w, block_y + h), (255, 255, 255), thickness=3)

    if fname is not None:
        mosaic = cv2.resize(mosaic, (int(ns * w * 0.5), int(ns * h * 0.5)), interpolation=cv2.INTER_AREA)
        cv2.imwrite(fname, cv2.cvtColor(mosaic, cv2.COLOR_BGR2RGB))

    return mosaic


def plot_lr_scheduler(optimizer, scheduler, epochs=300):
    # Plot LR simulating training for full epochs
    optimizer, scheduler = copy(optimizer), copy(scheduler)  # do not modify originals
    y = []
    for _ in range(epochs):
        scheduler.step()
        y.append(optimizer.param_groups[0]['lr'])
    plt.plot(y, '.-', label='LR')
    plt.xlabel('epoch')
    plt.ylabel('LR')
    plt.tight_layout()
    plt.savefig('LR.png', dpi=200)


def plot_test_txt():  # from utils.utils import *; plot_test()
    # Plot test.txt histograms
    x = np.loadtxt('test.txt', dtype=np.float32)
    box = xyxy2xywh(x[:, :4])
    cx, cy = box[:, 0], box[:, 1]

    fig, ax = plt.subplots(1, 1, figsize=(6, 6), tight_layout=True)
    ax.hist2d(cx, cy, bins=600, cmax=10, cmin=0)
    ax.set_aspect('equal')
    plt.savefig('hist2d.png', dpi=300)

    fig, ax = plt.subplots(1, 2, figsize=(12, 6), tight_layout=True)
    ax[0].hist(cx, bins=600)
    ax[1].hist(cy, bins=600)
    plt.savefig('hist1d.png', dpi=200)


def plot_targets_txt():  # from utils.utils import *; plot_targets_txt()
    # Plot targets.txt histograms
    x = np.loadtxt('targets.txt', dtype=np.float32).T
    s = ['x targets', 'y targets', 'width targets', 'height targets']
    fig, ax = plt.subplots(2, 2, figsize=(8, 8), tight_layout=True)
    ax = ax.ravel()
    for i in range(4):
        ax[i].hist(x[i], bins=100, label='%.3g +/- %.3g' % (x[i].mean(), x[i].std()))
        ax[i].legend()
        ax[i].set_title(s[i])
    plt.savefig('targets.jpg', dpi=200)


def plot_labels(labels):
    # plot dataset labels
    c, b = labels[:, 0], labels[:, 1:].transpose()  # classees, boxes

    def hist2d(x, y, n=100):
        xedges, yedges = np.linspace(x.min(), x.max(), n), np.linspace(y.min(), y.max(), n)
        hist, xedges, yedges = np.histogram2d(x, y, (xedges, yedges))
        xidx = np.clip(np.digitize(x, xedges) - 1, 0, hist.shape[0] - 1)
        yidx = np.clip(np.digitize(y, yedges) - 1, 0, hist.shape[1] - 1)
        return hist[xidx, yidx]

    fig, ax = plt.subplots(2, 2, figsize=(8, 8), tight_layout=True)
    ax = ax.ravel()
    ax[0].hist(c, bins=int(c.max() + 1))
    ax[0].set_xlabel('classes')
    ax[1].scatter(b[0], b[1], c=hist2d(b[0], b[1], 90), cmap='jet')
    ax[1].set_xlabel('x')
    ax[1].set_ylabel('y')
    ax[2].scatter(b[2], b[3], c=hist2d(b[2], b[3], 90), cmap='jet')
    ax[2].set_xlabel('width')
    ax[2].set_ylabel('height')
    plt.savefig('labels.png', dpi=200)


def plot_evolution_results(hyp):  # from utils.utils import *; plot_evolution_results(hyp)
    # Plot hyperparameter evolution results in evolve.txt
    x = np.loadtxt('evolve.txt', ndmin=2)
    f = fitness(x)
    # weights = (f - f.min()) ** 2  # for weighted results
    fig = plt.figure(figsize=(12, 10), tight_layout=True)
    matplotlib.rc('font', **{'size': 8})
    for i, (k, v) in enumerate(hyp.items()):
        y = x[:, i + 7]
        # mu = (y * weights).sum() / weights.sum()  # best weighted result
        mu = y[f.argmax()]  # best single result
        plt.subplot(4, 5, i + 1)
        plt.plot(mu, f.max(), 'o', markersize=10)
        plt.plot(y, f, '.')
        plt.title('%s = %.3g' % (k, mu), fontdict={'size': 9})  # limit to 40 characters
        print('%15s: %.3g' % (k, mu))
    plt.savefig('evolve.png', dpi=200)


def plot_results_overlay(start=0, stop=0):  # from utils.utils import *; plot_results_overlay()
    # Plot training results files 'results*.txt', overlaying train and val losses
    s = ['train', 'train', 'train', 'Precision', 'mAP@0.5', 'val', 'val', 'val', 'Recall', 'F1']  # legends
    t = ['GIoU', 'Objectness', 'Classification', 'P-R', 'mAP-F1']  # titles
    for f in sorted(glob.glob('results*.txt') + glob.glob('../../Downloads/results*.txt')):
        results = np.loadtxt(f, usecols=[2, 3, 4, 8, 9, 12, 13, 14, 10, 11], ndmin=2).T
        n = results.shape[1]  # number of rows
        x = range(start, min(stop, n) if stop else n)
        fig, ax = plt.subplots(1, 5, figsize=(14, 3.5), tight_layout=True)
        ax = ax.ravel()
        for i in range(5):
            for j in [i, i + 5]:
                y = results[j, x]
                if i in [0, 1, 2]:
                    y[y == 0] = np.nan  # dont show zero loss values
                ax[i].plot(x, y, marker='.', label=s[j])
            ax[i].set_title(t[i])
            ax[i].legend()
            ax[i].set_ylabel(f) if i == 0 else None  # add filename
        fig.savefig(f.replace('.txt', '.png'), dpi=200)


def plot_results(start=0, stop=0, bucket='', id=()):  # from utils.utils import *; plot_results()
    # Plot training 'results*.txt' as seen in https://github.com/ultralytics/yolov3#training
    fig, ax = plt.subplots(2, 5, figsize=(12, 6), tight_layout=True)
    ax = ax.ravel()
    s = ['GIoU', 'Objectness', 'Classification', 'Precision', 'Recall',
         'val GIoU', 'val Objectness', 'val Classification', 'mAP@0.5', 'F1']
    if bucket:
        os.system('rm -rf storage.googleapis.com')
        files = ['https://storage.googleapis.com/%s/results%g.txt' % (bucket, x) for x in id]
    else:
        files = glob.glob('results*.txt') + glob.glob('../../Downloads/results*.txt')
    for f in sorted(files):
        try:
            results = np.loadtxt(f, usecols=[2, 3, 4, 8, 9, 12, 13, 14, 10, 11], ndmin=2).T
            n = results.shape[1]  # number of rows
            x = range(start, min(stop, n) if stop else n)
            for i in range(10):
                y = results[i, x]
                if i in [0, 1, 2, 5, 6, 7]:
                    y[y == 0] = np.nan  # dont show zero loss values
                    # y /= y[0]  # normalize
                ax[i].plot(x, y, marker='.', label=Path(f).stem, linewidth=2, markersize=8)
                ax[i].set_title(s[i])
                # if i in [5, 6, 7]:  # share train and val loss y axes
                #     ax[i].get_shared_y_axes().join(ax[i], ax[i - 5])
        except:
            print('Warning: Plotting error for %s, skipping file' % f)

    ax[1].legend()
    fig.savefig('results.png', dpi=200)



'''关键点加权 loss模块————WST Head'''
def check_wrong(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                wst_pre, wst_d_ps, pred, temp_c, key='Nan'):
    print('\n \n =================== Have %s =================== ' % (key))
    print(' Have nan:       loss[%s] = %s ' % (ss, loss[ss]))
    print(' Similar:        loss_y0[%s] = %s ' % (ss, loss_y0[ss]))
    print(' Different:      loss_y1[%s] = %s ' % (ss, loss_y1[ss]))
    print(' Label:          wst_target[%s] = %s ' % (ss, wst_target[ss]))
    print(' pred-BN-dis-sg: pred_prob[%s] = %s ' % (ss, pred_prob[ss]))
    print(' pred-BN-dis:    wst_pre[%s] = %s ' % (ss, wst_pre[ss]))

    for temp_i in range(nb):
        temp_s = ((nb - 1) + (nb - 1 - (temp_i - 1))) * temp_i * 0.5
        # print("\n temp_s == ", temp_s)
        if temp_s > ss:
            temp_ss = ((nb - 1) + (nb - 1 - (temp_i - 2))) * (temp_i - 1) * 0.5
            # print("\n temp_ss == ", temp_ss)
            temp_j = int(ss - temp_ss + temp_i)
            # print("\n temp_j == ", temp_j)
            print("\n ************************ N0. %s************************************ " % (temp_c))
            print(' ----------- After BN -----------')
            print(' Have %s: \n ni-wst_d_ps[%s] = %s ' % (key, temp_i - 1, wst_d_ps[temp_i - 1, 5:]))
            print(' nj-wst_d_ps[%s] = %s ' % (temp_j, wst_d_ps[temp_j, 5:]))
            print(' ----------- Before BN -----------')
            print(' Have %s: \n ni-pred[%s] = %s ' % (key, temp_i - 1, pred[temp_i - 1, 5:]))
            print(' nj-pred[%s] = %s ' % (temp_j, pred[temp_j, 5:]))
            break
def check_wrong_cos(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                    wst_pre, wst_d, wst_temp, wst_l2_temp, wst_d_temp,
                    wst_d_ps, pred, temp_c, key='Nan'):
    print('\n \n =================== Have %s =================== ' % (key))
    print(' Have nan:        loss[%s] = %s ' % (ss, loss[ss]))
    print(' Similar:         loss_y0[%s] = %s ' % (ss, loss_y0[ss]))
    print(' Different:       loss_y1[%s] = %s ' % (ss, loss_y1[ss]))
    print(' Label:           wst_target[%s] = %s ' % (ss, wst_target[ss]))
    print(' pred-BN-dis-sg:  pred_prob[%s] = %s ' % (ss, pred_prob[ss]))
    print(' pred-BN-(1-cos): wst_pre[%s] = %s ' % (ss, wst_pre[ss]))
    print(' pred-BN-cos:     wst_d[%s] = %s ' % (ss, wst_d[ss]))

    for temp_i in range(nb):
        temp_s = ((nb - 1) + (nb - 1 - (temp_i - 1))) * temp_i * 0.5
        # print("\n temp_s == ", temp_s)
        if temp_s > ss:
            temp_ss = ((nb - 1) + (nb - 1 - (temp_i - 2))) * (temp_i - 1) * 0.5
            # print("\n temp_ss == ", temp_ss)
            temp_j = int(ss - temp_ss + temp_i)
            # print("\n temp_j == ", temp_j)
            print("\n ************************ N0. %s************************************ " % (temp_c))
            print(' ----------- Cosine(a,b) -----------')
            print(' Have %s: \n nij-wst_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                               wst_temp[temp_i - 1, temp_j]))
            print(' ----------- |a|*|b| -----------')
            print(' Have %s: \n nij-wst_l2_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                                  wst_l2_temp[temp_i - 1, temp_j]))
            print(' ----------- Sum£¨a*b£© -----------')
            print(' Have %s: \n nij-wst_d_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                                 wst_d_temp[temp_i - 1, temp_j]))
            print(' ----------- After BN -----------')
            print(' Have %s: \n ni-wst_d_ps[%s] = %s ' % (key, temp_i - 1, wst_d_ps[temp_i - 1, 5:]))
            print(' nj-wst_d_ps[%s] = %s ' % (temp_j, wst_d_ps[temp_j, 5:]))
            print(' ----------- Before BN -----------')
            print(' Have %s: \n ni-pred[%s] = %s ' % (key, temp_i - 1, pred[temp_i - 1, 5:]))
            print(' nj-pred[%s] = %s ' % (temp_j, pred[temp_j, 5:]))
            break
class Batch_Norm_WST(nn.Module):
    def __init__(self, units, bias=True):
        super(Batch_Norm_WST, self).__init__()
        # self.weights = nn.Parameter(torch.randn(in_units, units))
        self.weight = nn.Parameter(torch.Tensor(units))
        if bias:
            # self.bias = nn.Parameter(torch.randn(units))
            self.bias = nn.Parameter(torch.Tensor(units))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()   # 初始化参数

    # 初始化参数
    def reset_parameters(self):
        # Not a very smart way to initialize weights
        self.weight.data.uniform_(0., 2.)
        if self.bias is not None:
            self.bias.data.uniform_(-1., 1.)

    def forward(self, x, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
        eps = 1e-5
        x_mean = torch.mean(x, dim=bn_dim)
        # x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
        x_var = torch.mean((x - x_mean) ** 2, dim=bn_dim)
        if is_train == True:
            moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
            moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
            # x_hat = (x - x_mean) / (x_var + eps)
            x_hat = (x - x_mean) / (torch.sqrt(x_var) + eps)
            return self.weight * x_hat + self.bias, moving_means.detach(), moving_vars.detach()
        else:
            x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
            x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
            x_hat = (x - x_mean) / (x_var + eps)
            return self.weight * x_hat + self.bias, moving_means, moving_vars

    def backward(self, retain_graph=True):
        self.weight.backward(retain_graph=retain_graph)
        if self.bias is not None:
            self.bias.backward(retain_graph=retain_graph)
            return self.weight, self.bias
        else:
            return self.weight


'''类别均值向量全局差值 loss模块——WST Head'''
class RoyFocalLoss_v3_WH(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, BN_WST, gamma=2.0, alpha=0.25, omg=0.1, reduction='sum'):
        super(RoyFocalLoss_v3_WH, self).__init__()
        # self.gamma = torch.tensor(gamma, device='cuda:0')
        self.gamma = gamma
        # self.alpha = torch.tensor(alpha, device='cuda:0')
        self.alpha = alpha
        # self.omg = torch.tensor(omg, device='cuda:0')
        self.omg = omg
        self.reduction = reduction
        self.BN_WST = BN_WST

    def batch_norm_v3(self, x, gamma, beta, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
        eps = 1e-5
        x_mean = torch.mean(x, dim=bn_dim)
        x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
        if is_train == True:
            moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
            moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
            x_hat = (x - x_mean) / (x_var + eps)
            return gamma * x_hat + beta, moving_means.detach(), moving_vars.detach()
        else:
            x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
            x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
            x_hat = (x - x_mean) / (x_var + eps)
            return gamma * x_hat + beta, moving_means, moving_vars

    def cf_v3(eslf, x, c_means, moving_momentum, is_train=True, c_dim=0):
        x_mean = torch.mean(x, dim=c_dim)
        c_means = moving_momentum * c_means + (1. - moving_momentum) * x_mean
        if is_train == True:
            return c_means, c_means.detach()
        else:
            return c_means, c_means

    # ===> (-1, 0)
    def bn_linear(self, x, x_max, x_min):
        x = x - x_max - 0.001
        x = torch.div(x, (0.002 + (x_max + 0.001 - x_min)))    # (-1, 0)
        return x

    # ===> (-1, 1)
    def compute_cosine_L2_v3(self, ps_1, ps_2):
        '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
        wst_d_a = ps_1.unsqueeze(dim=1)
        wst_d_b = ps_2.unsqueeze(dim=0)
        wst_d_temp = wst_d_a * wst_d_b
        wst_d_temp = wst_d_temp.sum(dim=2)

        wst_l1_a = torch.norm(ps_1, dim=1, p=2)
        wst_l1_a = wst_l1_a.unsqueeze(dim=1)
        wst_l1_b = torch.norm(ps_2, dim=1, p=2)
        wst_l1_b = wst_l1_b.unsqueeze(dim=0)
        wst_l1_temp = wst_l1_a * wst_l1_b

        wst_temp = wst_d_temp / wst_l1_temp   #(-1, 1)
        wst_temp = wst_temp - 0.00001    #(-1, 1)  加上0.0001一个极小数是为了避免出现1.0的结果，这里有一个bug，不加极小数就会出现1.0的情况，具体原因未知
        temp_wst_temp = torch.max(wst_temp)
        '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''
        if temp_wst_temp > 1.0:
            print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
            print("wst_d_temp ===> ", wst_d_temp)
            print("wst_l1_temp ===> ", wst_l1_temp)
            print("\033[0m")
        return wst_temp

    def forward(self, cwg, pred, true, epoch, epochs, is_train,
                cf_means, cf_move, fe_means, fe_vars, fe_move, measure_loss='L1'):
        '''========================== Meta Learning ================================='''
        nb = pred.shape[0]
        _, k_t_id = torch.max(true, dim=1)  # find the class of every feature vec
        ps = pred.clone()

        '''============ 特征向量预处理 BN操作 ============'''
        # gamma_bn = torch.ones(1, device='cuda:0')
        # beta_bn = torch.zeros(1, device='cuda:0')
        gamma_bn = torch.ones(1)
        beta_bn = torch.zeros(1)
        # 各特征维度做BN，在单一特征维度上保持正态分布
        m_m = torch.tensor((10.0 * epoch / epochs))
        m_mfe = torch.sigmoid(m_m) * fe_move
        if BN_WST_module == 'Done':
            ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :] = \
                self.BN_WST(ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :], m_mfe, is_train=is_train)

        new_ps = ps[:, 5:].clone()
        new_ps = torch.cat((k_t_id[:, None], new_ps), 1)  # feature vec里在第一个元素上加入类别标签

        '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
        '''ct：统计当前ps序列中所有出现的类别'''
        ct_idx = true.sum(dim=0)
        ct_idx = ct_idx.cpu().numpy()
        ct = np.argwhere(ct_idx != 0)
        ct = torch.from_numpy(ct)
        ct = ct.to(new_ps.device)

        '''
        idx_ci：当前ps序列中属于i类的行索引
        ps_ci：当前ps序列中属于i类的所以feature vecs的集合
        m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
        '''
        new_ps_n = new_ps.detach().cpu().numpy()  # 便于操作

        m_mcf = torch.sigmoid(m_m) * cf_move
        names = locals()  # 便于动态命名，使其变量名能与类别标签对应上
        for ci in range(cf_means.shape[1]):
            names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
            if names['idx_c%s' % ci].size != 0:
                names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
                names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
                names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
                # names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
                names['m_ps_c%s' % ci], cf_means[epoch, ci, :] = \
                    self.cf_v3(names['ps_c%s' % ci], cf_means[epoch, ci, :], m_mcf, is_train=is_train)

        '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
        for ci in range(ct.shape[0]):
            temp_ci = int(ct[ci, 0])
            if ci == 0:
                m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
            else:
                m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)

        '''====================== 计算差值 ======================'''
        ''' 
        rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
        idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
        '''
        for ci in range(ct.shape[0]):
            temp_ci = int(ct[ci, 0])
            if measure_loss == 'L1':
                names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
            elif measure_loss == 'L2':
                temp_aaa_subdis = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci])
                names['rd_c%s' % temp_ci] = torch.norm(temp_aaa_subdis, dim=1, p=2).unsqueeze(dim=1)
            elif measure_loss == 'Cosine':
                temp_aaa = self.compute_cosine_L2_v3(names['ps_c%s' % temp_ci],
                                                  names['m_ps_c%s' % temp_ci].unsqueeze(dim=0))    # (-1, 1)
                names['rd_c%s' % temp_ci] = (1.0 - temp_aaa).squeeze(dim=1)    # (0, 2)

                if torch.max(temp_aaa) > 1.0:
                    print("\n\033[1;33m training: ")
                    print("temp_aaa ===> ", temp_aaa)
                    print("temp_aaa ===> %.20f" % torch.max(temp_aaa))
                    print("\033[0m")

            if ci == 0:
                rd_cs_ps = names['rd_c%s' % temp_ci]
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = temp_idx
            else:
                rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

        ''' 
        rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1） 
        '''
        if measure_loss == 'L1':
            rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
        elif measure_loss == 'L2':
            rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=2)
        elif measure_loss == 'Cosine':
            temp_bbb = self.compute_cosine_L2_v3(ps[:, 5:], m_ps_cs)    # (-1, 1)
            rd_all = 1.0 - temp_bbb    # (0, 2)


        ''' 
        rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组 
        rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组 
        '''
        idx_ct_ps = idx_ct_ps.to(torch.int64)
        rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
        rt_all_ct_ps = torch.index_select(true, 0, idx_ct_ps)

        # cls_wst_gain
        if multi_task_module == 'Done':
            cwg_ct_ps = torch.index_select(cwg, 0, idx_ct_ps)
            cwg_ct_ps = cwg_ct_ps.unsqueeze(1)


        ''' 
        rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1） 
        rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1） 
        '''
        rd_cs_ps_d0 = rd_cs_ps[None, :].t()    # (0, 2)
        temp_0_min = torch.min(rd_cs_ps_d0)
        temp_0_max = torch.max(rd_cs_ps_d0)
        rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)    # (-2, 2)

        if temp_0_min < 0.0:
            print("training \n rd_cs_ps ===> ", rd_cs_ps)

        if ct.shape[0] != 1:
            ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if ci == 0:
                    ci_idx = ci
                else:
                    ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:,
                                                     torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['cwg_c%s' % temp_ci] = cwg_ct_ps[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                if ci == 0:
                    temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
                    temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
                else:
                    temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(
                        names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
                    temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(
                        names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min

            ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d1_c%s' % temp_ci], temp_1_max, temp_1_min)   # (-1, 0)
                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_1_max + 0.001 - temp_1_min)))   # (-1, 0)
                    # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                    temp_1_mid = 0.5
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 3.0 * names['rd_cs_ps_d1_c%s' % temp_ci]   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = (names['rd_cs_ps_d1_c%s_p' % temp_ci] - 6.0) * names['cwg_c%s' % temp_ci] + 6.0

                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])


                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)
                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                    temp_0_mid = 0.5
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = (names['rd_cs_ps_d0_c%s_p' % temp_ci] + 6.0) * names['cwg_c%s' % temp_ci] - 6.0

                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                if ci == 0:
                    rd_cs_ps_d1_ct_p = names['rd_cs_ps_d1_c%s_p' % temp_ci]
                    rd_cs_ps_d0_ct_p = names['rd_cs_ps_d0_c%s_p' % temp_ci]
                else:
                    rd_cs_ps_d1_ct_p = torch.cat((rd_cs_ps_d1_ct_p, names['rd_cs_ps_d1_c%s_p' % temp_ci]), 0)
                    rd_cs_ps_d0_ct_p = torch.cat((rd_cs_ps_d0_ct_p, names['rd_cs_ps_d0_c%s_p' % temp_ci]), 0)

            # print('\n rd_cs_ps_d1_ct_p \n', rd_cs_ps_d1_ct_p)
            # print('\n rd_cs_ps_d0_ct_p \n', rd_cs_ps_d0_ct_p)

            ''' BCELoss Loss '''
            loss_y1 = - torch.log(rd_cs_ps_d1_ct_p)
            loss_y0 = - torch.log(1 - rd_cs_ps_d0_ct_p)
            ''' Focal Loss '''
            # loss_y1 = - (1 - rd_cs_ps_d1_ct_p) ** self.gamma * self.alpha * torch.log(rd_cs_ps_d1_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p)
            # loss_y1 = - (1 - rd_cs_ps_d1_ct_p) ** self.gamma * self.alpha * torch.log(rd_cs_ps_d1_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p) - \
            #           (1 - rd_cs_ps_d0_ct_p) ** self.gamma * self.omg * torch.log(rd_cs_ps_d0_ct_p)
            loss = loss_y0 + loss_y1


        else:
            ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if ci == 0:
                    ci_idx = ci
                else:
                    ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['cwg_c%s' % temp_ci] = cwg_ct_ps[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

            ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)
                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                    temp_0_mid = 0.5
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                     names['rd_cs_ps_d0_c%s_p' % temp_ci] = (names['rd_cs_ps_d0_c%s_p' % temp_ci] + 6.0) * names['cwg_c%s' % temp_ci] - 6.0

                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])

                if ci == 0:
                    rd_cs_ps_d0_ct_p = names['rd_cs_ps_d0_c%s_p' % temp_ci]
                else:
                    rd_cs_ps_d0_ct_p = torch.cat((rd_cs_ps_d0_ct_p, names['rd_cs_ps_d0_c%s_p' % temp_ci]), 0)

            # print('\n rd_cs_ps_d0_ct_p \n', rd_cs_ps_d0_ct_p)

            ''' BCELoss Loss '''
            loss_y0 = - torch.log(1 - rd_cs_ps_d0_ct_p)
            ''' Focal Loss '''
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p) - \
            #           (1 - rd_cs_ps_d0_ct_p) ** self.gamma * self.omg * torch.log(rd_cs_ps_d0_ct_p)
            loss = loss_y0


        nl = loss.shape[0]
        ii = loss.shape[1]

        if torch.isnan(loss.sum()):
            print('loss_sum = %s ' % (loss.sum()))
            print('loss_sum_y0 = %s ' % (loss_y0.sum()))
            print('loss_sum_y1 = %s ' % (loss_y1.sum()))

        if self.reduction == 'elementwise_mean':
            return loss.mean(), nl
        elif self.reduction == 'sum':
            return loss.sum(), nl, ii
        else:  # 'none'
            return loss, nl


'''************* Meta Learning —— 对比学习wst Head + yolo Head —— loss *************'''
def ohem_loss(batch_size, cls_loss, loc_loss):
    """    Arguments:
     batch_size (int): number of sampled rois for bbox head training
     loc_pred (FloatTensor): [R, 4], location of positive rois
     loc_target (FloatTensor): [R, 4], location of positive rois
     pos_mask (FloatTensor): [R], binary mask for sampled positive rois
     cls_pred (FloatTensor): [R, C]
     cls_target (LongTensor): [R]
     Returns:
           cls_loss, loc_loss (FloatTensor)
    """
    loss = cls_loss + loc_loss    # 然后对分类和回归loss求和
    sorted_ohem_loss, idx = torch.sort(loss, descending=True)    # 再对loss进行降序排列
    keep_num = min(sorted_ohem_loss.size()[0], batch_size)    # 得到需要保留的loss数量

    if keep_num < sorted_ohem_loss.size()[0]:    # 这句的作用是如果保留数目小于现有loss总数，则进行筛选保留，否则全部保留
        keep_num = max(int(0.75 * sorted_ohem_loss.size()[0]), batch_size)
        keep_idx_cuda = idx[:keep_num]  # 保留到需要keep的数目
        cls_loss = cls_loss[keep_idx_cuda]
        loc_loss = loc_loss[keep_idx_cuda]  # 分类和回归保留相同的数目
        cls_loss = cls_loss.sum() / keep_num
        loc_loss = loc_loss.sum() / keep_num  # 然后分别对分类和回归loss求均值
        return cls_loss, loc_loss
    else:
        cls_loss = cls_loss.sum() / loss.shape[0]
        loc_loss = loc_loss.sum() / loss.shape[0]
        return cls_loss, loc_loss
def compute_loss_WH(wst_p, p, targets, model, epoch, epochs, cf_means, cf_move,
                     fe_means, fe_vars, fe_move, is_train=True):  # predictions, targets, model
    ft = torch.cuda.FloatTensor if p[0].is_cuda else torch.Tensor
    lcls, lbox, lobj = ft([0]), ft([0]), ft([0])
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    h = model.hyp  # hyperparameters
    red = 'mean'  # Loss reduction (sum or mean)

    # Define criteria
    if OHEM_module == 'None':
        BCEcls = nn.BCEWithLogitsLoss(pos_weight=ft([h['cls_pw']]), reduction=red)
        BCEobj = nn.BCEWithLogitsLoss(pos_weight=ft([h['obj_pw']]), reduction=red)
    elif OHEM_module == 'Done':
        '''-------- 加入 OHEM 模块 --------'''
        red_cls = 'none'  # Loss reduction (sum or mean)
        BCEcls = nn.BCEWithLogitsLoss(pos_weight=ft([h['cls_pw']]), reduction=red_cls)
        BCEobj = nn.BCEWithLogitsLoss(pos_weight=ft([h['obj_pw']]), reduction=red)


    # class label smoothing https://arxiv.org/pdf/1902.04103.pdf eqn 3
    cp, cn = smooth_BCE(eps=0.0)


    ''' =============== WST Learning——wst =============== '''  # 3
    lwst = ft([0])
    if BN_WST_module == 'Done':
        if loss_module_name == 'v3':
            BCEwst_v3_WH = RoyFocalLoss_v3_WH(model.BN_WST)  # v3

    else:
        BN_WST = 1.0   # 没啥具体意义，用来占位
        if loss_module_name == 'v3':
            BCEwst_v3_WH = RoyFocalLoss_v3_WH(BN_WST)  # v3


    # focal loss
    g = h['fl_gamma']  # focal loss gamma
    if g > 0:
        BCEcls, BCEobj = FocalLoss(BCEcls, g), FocalLoss(BCEobj, g)

    # per output
    # nt = 0  # targets
    # for i, pi in enumerate(p):  # layer index, layer predictions
    #     '''================================ 每个anchor的loss计算 ====================================='''
    #     b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
    #     # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
    #
    #     tobj = torch.zeros_like(pi[..., 0])  # target obj  # tobj用于存放置信度
    #     # print("pi[..., 0] ===>", pi[..., 0])
    #
    #     nb = b.shape[0]  # number of targets
    #     if nb:
    #         nt += nb  # cumulative targets
    #         ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets
    #
    #         # GIoU
    #         pxy = ps[:, :2].sigmoid()
    #         pwh = ps[:, 2:4].exp().clamp(max=1E3) * anchors[i]
    #         pbox = torch.cat((pxy, pwh), 1)  # predicted box
    #         giou = bbox_iou(pbox.t(), tbox[i], x1y1x2y2=False, GIoU=True)  # giou(prediction, target)
    #         if OHEM_module == 'None':
    #             lbox += (1.0 - giou).sum() if red == 'sum' else (1.0 - giou).mean()  # giou loss
    #         elif OHEM_module == 'Done':
    #             '''-------- 加入 OHEM 模块 --------'''
    #             lbox_temp = (1.0 - giou) if red_cls == 'none' else (1.0 - giou).mean()  # giou loss
    #
    #
    #         # Obj
    #         tobj[b, a, gj, gi] = (1.0 - model.gr) + model.gr * giou.detach().clamp(0).type(tobj.dtype)  # giou ratio
    #
    #         # Class
    #         if model.nc > 1:  # cls loss (only if multiple classes)
    #             t = torch.full_like(ps[:, 5:], cn)  # targets
    #             t[range(nb), tcls[i]] = cp
    #             if OHEM_module == 'None':
    #                 lcls += BCEcls(ps[:, 5:], t)  # BCE
    #             elif OHEM_module == 'Done':
    #                 '''-------- 加入 OHEM 模块 --------'''
    #                 lcls_temp = BCEcls(ps[:, 5:], t).sum(1)  # BCE
    #
    #
    #         if OHEM_module == 'Done':
    #             '''-------- 加入 OHEM 模块 --------'''
    #             lcls_temp, lbox_temp = ohem_loss(16, lcls_temp, lbox_temp)
    #             lcls += lcls_temp
    #             lbox += lbox_temp
    #
    #
    #     lobj += BCEobj(pi[..., 4], tobj)  # obj loss
    #
    #
    # ''' =============== WST Learning——wst =============== '''  # 3
    # for i, wst_pi in enumerate(wst_p):  # layer index, layer predictions
    #     '''================================ 每个anchor的loss计算 ====================================='''
    #     b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
    #     # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
    #
    #     nb = b.shape[0]  # number of targets
    #     if nb:
    #         nt += nb  # cumulative targets
    #         wst_ps = wst_pi[b, a, gj, gi]  # prediction subset corresponding to targets
    #
    #         # Class
    #         if model.nc > 1:  # cls loss (only if multiple classes)
    #             wst_t = torch.full_like(wst_ps[:, :9], cn)  # targets
    #             wst_t[range(nb), tcls[i]] = cp
    #
    #
    #     if nb > 1 and epoch <= 500:
    #         # t_wst_s = torch_utils.time_synchronized()
    #         if wst_loss == 'v1':
    #             '''-------- v1 关键点加权差值方法 wst_k , 直接差值也在这里 wst--------'''
    #             loss_wst, nl_wst, ii_wst = BCEwst_v1_WH(wst_ps, wst_t, epoch, epochs, is_train,
    #                                                     fe_means, fe_vars, fe_move, measure_loss=measure)
    #             if nl_wst != 0:
    #                 loss_wst = loss_wst / nl_wst
    #                 lwst += loss_wst
    #             else:
    #                 lwst= lwst + 1e-5
    #
    #         elif wst_loss == 'v3':
    #             '''-------- v3 类别均值向量全局差值方法 wst_cmgl--------'''
    #             loss_wst, nl_wst, ii_wst = BCEwst_v3_WH(wst_ps, wst_t, epoch, epochs, is_train,
    #                                                     cf_means, cf_move, fe_means, fe_vars, fe_move, measure_loss=measure)
    #             if nl_wst != 0 and ii_wst != 0:
    #                 loss_wst = loss_wst / nl_wst
    #                 loss_wst = loss_wst / ii_wst
    #                 lwst += loss_wst
    #             else:
    #                 lwst = lwst + 1e-5
    #         # t_wst_e = torch_utils.time_synchronized() - t_wst_s
    #         # print("\n t_wst用时 ===> ", t_wst_e)


    ''' ############################# new way ############################# '''
    # per output
    nt = 0  # targets
    for i, (pi, wst_pi) in enumerate(zip(p, wst_p)):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))

        tobj = torch.zeros_like(pi[..., 0])  # target obj  # tobj用于存放置信度
        # print("pi[..., 0] ===>", pi[..., 0])

        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            wst_ps = wst_pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # GIoU
            pxy = ps[:, :2].sigmoid()
            pwh = ps[:, 2:4].exp().clamp(max=1E3) * anchors[i]
            pbox = torch.cat((pxy, pwh), 1)  # predicted box
            giou = bbox_iou(pbox.t(), tbox[i], x1y1x2y2=False, GIoU=True)  # giou(prediction, target)
            if OHEM_module == 'None':
                lbox += (1.0 - giou).sum() if red == 'sum' else (1.0 - giou).mean()  # giou loss
            elif OHEM_module == 'Done':
                '''-------- 加入 OHEM 模块 --------'''
                lbox_temp = (1.0 - giou) if red_cls == 'none' else (1.0 - giou).mean()  # giou loss

            # Obj
            tobj[b, a, gj, gi] = (1.0 - model.gr) + model.gr * giou.detach().clamp(0).type(tobj.dtype)  # giou ratio

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp

                wst_t = torch.full_like(wst_ps[:, :9], cn)  # targets
                wst_t[range(nb), tcls[i]] = cp

                if OHEM_module == 'None':
                    lcls += BCEcls(ps[:, 5:], t)  # BCE
                elif OHEM_module == 'Done':
                    '''-------- 加入 OHEM 模块 --------'''
                    lcls_temp = BCEcls(ps[:, 5:], t).sum(1)  # BCE

            if OHEM_module == 'Done':
                '''-------- 加入 OHEM 模块 --------'''
                lcls_temp, lbox_temp = ohem_loss(16, lcls_temp, lbox_temp)
                lcls += lcls_temp
                lbox += lbox_temp

        lobj += BCEobj(pi[..., 4], tobj)  # obj loss

        if nb > 1 and epoch <= 500:
            # t_wst_s = torch_utils.time_synchronized()
            if multi_task_module == 'Done':
                # cls_wst_gain
                check_ps = ps[:, 5:].detach().sigmoid()  # sigmoid
                check_t = t.detach()
                _, ct = torch.max(check_t, dim=1)
                _, cps = torch.max(check_ps, dim=1)
                ct = ct.to(float)
                cps = cps.to(float)
                # global cls_wst_gain
                cls_wst_gain = ct - cps
                ones_cwg = torch.ones_like(cls_wst_gain)
                others_cwg = 0.85 * ones_cwg
                cls_wst_gain = torch.where(cls_wst_gain == 0.0, others_cwg, ones_cwg)
            else:
                cls_wst_gain = 1.0


            if wst_loss == 'v3':
                '''-------- v3 类别均值向量全局差值方法 wst_cmgl--------'''
                loss_wst, nl_wst, ii_wst = BCEwst_v3_WH(cls_wst_gain, wst_ps, wst_t, epoch, epochs, is_train, cf_means, cf_move, fe_means, fe_vars, fe_move, measure_loss=measure)
                if nl_wst != 0 and ii_wst != 0:
                    loss_wst = loss_wst / nl_wst
                    loss_wst = loss_wst / ii_wst
                    lwst += loss_wst
                else:
                    lwst = lwst + 1e-5
            # t_wst_e = torch_utils.time_synchronized() - t_wst_s
            # print("\n t_wstÓÃÊ± ===> ", t_wst_e)



    lbox *= h['giou']
    lobj *= h['obj']
    lcls *= h['cls']


    ''' =============== WST Learning——wst =============== '''  # 5
    if nt > 1:
        if epoch <= 500:
            lwst *= h['wst'] * h['wst_pw'] * wst_gain_1
            # print('if nt > 1: ===> lwst = ', lwst)
        else:
            lwst *= h['wst'] * h['wst_pw'] * wst_gain_2


    if red == 'sum':
        bs = tobj.shape[0]  # batch size
        g = 3.0  # loss gain_
        lobj *= g / bs
        if nt:
            lcls *= g / nt / model.nc
            lbox *= g / nt


    if OHEM_module == 'Done':
        '''-------- 加入 OHEM 模块 --------'''
        if red_cls == 'none':
            if nt:
                g = 1.0  # loss gain_
                lcls *= g / model.nc
                lbox *= g


    # loss = lbox + lobj + lcls
    # return loss, torch.cat((lbox, lobj, lcls, loss)).detach()
    ''' =============== WST Learning——wst =============== '''  # 2
    loss = lbox + lobj + lcls + lwst
    # print("lbox = %s \n lobj = %s \n lcls = %s \n lwst = %s" % (lbox, lobj, lcls, lwst))
    return loss, torch.cat((lbox, lobj, lcls, lwst, loss)).detach()


'''类别均值向量全局差值评价模块 ——WST Head'''
def batch_norm_wst(x, gamma, beta, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
    eps = 1e-5
    x_mean = torch.mean(x, dim=bn_dim)
    x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
    if is_train == True:
        moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
        moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means.detach(), moving_vars.detach()
    else:
        x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
        x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means, moving_vars
def cf_wst(x, c_means, moving_momentum, is_train=True, c_dim=0):
    x_mean = torch.mean(x, dim=c_dim)
    c_means = moving_momentum * c_means + (1. - moving_momentum) * x_mean
    if is_train == True:
        return c_means, c_means.detach()
    else:
        return c_means, c_means
def compute_cosine_L1(ps_1, ps_2):
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
    wst_d_a = ps_1.unsqueeze(dim=1)
    wst_d_b = ps_2.unsqueeze(dim=0)
    wst_d_temp = wst_d_a * wst_d_b
    wst_d_temp = wst_d_temp.sum(dim=2)

    wst_l1_a = torch.norm(ps_1, dim=1, p=1)
    wst_l1_a = wst_l1_a.unsqueeze(dim=1)
    wst_l1_b = torch.norm(ps_2, dim=1, p=1)
    wst_l1_b = wst_l1_b.unsqueeze(dim=0)
    wst_l1_temp = wst_l1_a * wst_l1_b

    wst_temp = wst_d_temp / wst_l1_temp
    wst_temp = wst_temp - 0.00001
    temp_wst_temp = torch.max(wst_temp)
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''

    if temp_wst_temp > 1.0:
        print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
        print("wst_d_temp ===> ", wst_d_temp)
        print("wst_l1_temp ===> ", wst_l1_temp)
        print("\033[0m")

    return wst_temp
# ===> (-1, 1)
def compute_cosine_L2(ps_1, ps_2):
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
    wst_d_a = ps_1.unsqueeze(dim=1)
    wst_d_b = ps_2.unsqueeze(dim=0)
    wst_d_temp = wst_d_a * wst_d_b
    wst_d_temp = wst_d_temp.sum(dim=2)

    wst_l2_a = torch.norm(ps_1, dim=1, p=2)
    wst_l2_a = wst_l2_a.unsqueeze(dim=1)
    wst_l2_b = torch.norm(ps_2, dim=1, p=2)
    wst_l2_b = wst_l2_b.unsqueeze(dim=0)
    wst_l2_temp = wst_l2_a * wst_l2_b

    wst_temp = wst_d_temp / wst_l2_temp
    wst_temp = wst_temp - 0.00001
    temp_wst_temp = torch.max(wst_temp)
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''

    if temp_wst_temp > 1.0:
        print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
        print("wst_d_temp ===> ", wst_d_temp)
        print("wst_l2_temp ===> ", wst_l2_temp)
        print("\033[0m")

    return wst_temp
# (-1, 0)
def test_bn_linear(x, x_max, x_min):
    x = x - x_max - 0.001
    x = torch.div(x, (0.002 + (x_max + 0.001 - x_min)))    # (-1, 0)
    return x
def wst_v3_test_WH(p, targets, model, epoch, epochs, cf_means, cf_move, fe_means, fe_vars, fe_move_test,
                   wst_check_w_01, wst_check_w_10, wst_check_0, wst_check_1, wst_check_w_01_ij, wst_check_w_10_ij):
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    # per output
    nt = 0  # targets
    cp, cn = smooth_BCE(eps=0.0)
    dy0_pre_right = 0
    dy1_pre_right = 0
    dy0_pre_wrong = 0
    dy1_pre_wrong = 0
    wst_check_w_01 = torch.from_numpy(wst_check_w_01).to(targets.device)
    wst_check_w_10 = torch.from_numpy(wst_check_w_10).to(targets.device)
    wst_check_w_01_ij = torch.from_numpy(wst_check_w_01_ij).to(targets.device)
    wst_check_w_10_ij = torch.from_numpy(wst_check_w_10_ij).to(targets.device)
    wst_check_0 = torch.from_numpy(wst_check_0).to(targets.device)
    wst_check_1 = torch.from_numpy(wst_check_1).to(targets.device)
    temp_rr = 0.0
    for i, pi in enumerate(p):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp

                '''========================== Meta Learning ================================='''
                if nb > 1:
                    '''====================== ps中的feature vecs做预处理 ======================'''
                    _, k_t_id = torch.max(t, dim=1)  # find the class of every feature vec
                    old_ps = ps.detach()
                    # old_ps[:, 5:] = old_ps[:, 5:].sigmoid()

                    '''============ 特征向量预处理 BN操作 ============'''
                    # gamma_bn = torch.ones(1, device='cuda:0')
                    # beta_bn = torch.zeros(1, device='cuda:0')
                    gamma_bn = torch.ones(1)
                    beta_bn = torch.zeros(1)
                    # 各特征维度做BN，在单一特征维度上保持正态分布
                    m_m = torch.tensor((10.0 * epoch / epochs))
                    m_mfe = torch.sigmoid(m_m) * fe_move_test
                    if BN_WST_module == 'Done':
                        old_ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :] = \
                            batch_norm_wst(old_ps[:, 5:], gamma_bn, beta_bn, fe_means[epoch, :],
                                                  fe_vars[epoch, :], m_mfe, is_train=False, bn_dim=0)

                    new_ps = old_ps[:, 5:]
                    new_ps = torch.cat((k_t_id[:, None], new_ps), 1)  # feature vec里在第一个元素上加入类别标签

                    '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
                    '''
                    ct：统计当前ps序列中所有出现的类别
                    '''
                    ct_idx = t.sum(dim=0)
                    ct_idx = ct_idx.cpu().numpy()
                    ct = np.argwhere(ct_idx != 0)
                    ct = torch.from_numpy(ct)
                    ct = ct.to(new_ps.device)
                    # print("\n ct \n", ct)

                    '''
                    idx_ci：当前ps序列中属于i类的行索引
                    ps_ci：当前ps序列中属于i类的所以feature vecs的集合
                    m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
                    '''
                    new_ps_n = new_ps.detach().cpu().numpy()  # 便于操作

                    m_mcf = torch.sigmoid(m_m) * cf_move
                    names = locals()  # 便于动态命名，使其变量名能与类别标签对应上
                    for ci in range(model.nc):
                        names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
                        if names['idx_c%s' % ci].size != 0:
                            names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
                            names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
                            names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
                            # names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)    # 簇心
                            names['m_ps_c%s' % ci], cf_means[epoch, ci, :] = \
                                cf_wst(names['ps_c%s' % ci], cf_means[epoch, ci, :], m_mcf, is_train=False, c_dim=0)

                    '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
                    for ci in range(ct.shape[0]):
                        temp_ci = int(ct[ci, 0])
                        if ci == 0:
                            m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
                            m_ps_cs_wcl = m_ps_ci_wcl
                        else:
                            m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
                            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
                            m_ps_cs_wcl = torch.cat((m_ps_cs_wcl, m_ps_ci_wcl), 0)

                    '''====================== 计算差值 ======================'''
                    '''
                    rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
                    idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
                    '''
                    for ci in range(ct.shape[0]):
                        temp_ci = int(ct[ci, 0])
                        if measure == 'L1':
                            names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci],
                                                                  names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
                        elif measure == 'L2':
                            temp_aaa_subdis = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci])
                            names['rd_c%s' % temp_ci] = torch.norm(temp_aaa_subdis, dim=1, p=2).unsqueeze(dim=1)
                        elif measure == 'Cosine':
                            temp_aaa = compute_cosine_L2(names['ps_c%s' % temp_ci],
                                                         names['m_ps_c%s' % temp_ci].unsqueeze(dim=0))    # (-1, 1)
                            names['rd_c%s' % temp_ci] = (1.0 - temp_aaa).squeeze(dim=1)    # (0, 2)

                            if torch.max(temp_aaa) > 1.0:
                                print("\n\033[1;33m training: ")
                                print("temp_aaa ===> ", temp_aaa)
                                print("temp_aaa ===> %.20f" % torch.max(temp_aaa))
                                print("\033[0m")


                        if ci == 0:
                            rd_cs_ps = names['rd_c%s' % temp_ci]
                            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                            idx_ct_ps = temp_idx
                        else:
                            rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
                            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                            idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

                    '''
                    rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1）
                    '''
                    if measure == 'L1':
                        rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
                    elif measure == 'L2':
                        rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=2)
                    elif measure == 'Cosine':
                        temp_bbb = compute_cosine_L2(ps[:, 5:], m_ps_cs)    # (-1, 1)
                        rd_all = 1.0 - temp_bbb    # (0, 2)


                    '''
                    rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
                    rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
                    '''
                    idx_ct_ps = idx_ct_ps.to(torch.int64)
                    rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
                    rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)

                    '''
                    rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
                    rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
                    '''
                    rd_cs_ps_d0 = rd_cs_ps[None, :].t()    # (0, 2)
                    temp_0_min = torch.min(rd_cs_ps_d0)
                    temp_0_max = torch.max(rd_cs_ps_d0)
                    rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)    # (-2, 2)


                    # if temp_0_min < 0.0:
                    #     print("testing \n rd_cs_ps ===> ", rd_cs_ps)


# need change because ct maybe equel 1
                    if ct.shape[0] != 1:
                        temp_rr += 1.0
                        ''' 
                        rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if ci == 0:
                                ci_idx = ci
                            else:
                                ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                            names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                            names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                            names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(
                                names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
                            names['dy1_c%s_ct' % temp_ci] = ct[torch.arange(ct.size(0)) != ci]
                            # print("\n names['dy1_c%s_ct] = %s \n" % (temp_ci, names['dy1_c%s_ct' % temp_ci]))
                            if ci == 0:
                                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
                                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
                            else:
                                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(
                                    names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
                                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(
                                    names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min

                        ''' 
                        rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
                                if measure == 'L1' or measure == 'L2':
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d1_c%s' % temp_ci], temp_1_max, temp_1_min)   # (-1, 0)
                                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
                                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci],
                                    #                                                  (0.002 + (temp_1_max + 0.001 - temp_1_min)))   # (-1, 0)

                                    # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                                    temp_1_mid = 0.5
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)   # (-6, 6)

                                elif measure == 'Cosine':
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 3.0 * names['rd_cs_ps_d1_c%s' % temp_ci]   # (-6, 6)

                                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])


                                if measure == 'L1' or measure == 'L2':
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)

                                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                                    temp_0_mid = 0.5
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                                elif measure == 'Cosine':
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                                names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
                                wst_check_0[temp_ci, temp_ci] += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]

                                dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
                                dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()

                                names['dy1_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d1_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2

                                dy1_pre_right += names['dy1_pre_c%s_p' % temp_ci].sum()
                                dy1_pre_wrong += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0] * \
                                                 names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[1] - names['dy1_pre_c%s_p' % temp_ci].sum()

                                for cj in range(names['dy1_c%s_ct' % temp_ci].shape[1]):
                                    temp_cj = int(names['dy1_c%s_ct' % temp_ci][cj, 0])
                                    wst_check_1[temp_ci, temp_cj] += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0]

                                    for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
                                        if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
                                            wst_check_w_01[temp_ci, temp_ci] += 1

                                            if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                                                wst_check_w_01_ij[temp_ci, temp_cj] += 1

                                        if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                                            wst_check_w_10[temp_ci, temp_cj] += 1

                                            if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 0.0:
                                                wst_check_w_10_ij[temp_cj, temp_ci] += 1

                    else:
                        ''' 
                        rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if ci == 0:
                                ci_idx = ci
                            else:
                                ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                            names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                        ''' 
                        rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if measure == 'L1' or measure == 'L2':
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                                # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                                # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                                #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)

                                # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                                temp_0_mid = 0.5
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                            elif measure == 'Cosine':
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                            names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                            names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
                            wst_check_0[temp_ci, temp_ci] += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]

                            dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
                            dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()

                            for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
                                if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
                                    wst_check_w_01[temp_ci, temp_ci] += 1

    m_ps_cs_wcl = m_ps_cs_wcl.cpu().numpy()

    wst_check_w_01 = wst_check_w_01.cpu().numpy()
    wst_check_w_10 = wst_check_w_10.cpu().numpy()
    wst_check_w_01_ij = wst_check_w_01_ij.cpu().numpy()
    wst_check_w_10_ij = wst_check_w_10_ij.cpu().numpy()
    wst_check_0 = wst_check_0.cpu().numpy()
    wst_check_1 = wst_check_1.cpu().numpy()

    if temp_rr == 0.0:
        dy0_pre_right = dy0_pre_right.cpu().numpy()
        dy0_pre_wrong = dy0_pre_wrong.cpu().numpy()
        dy1_pre_right = np.zeros_like(dy0_pre_right)
        dy1_pre_wrong = np.zeros_like(dy0_pre_wrong)
    else:
        dy0_pre_right = dy0_pre_right.cpu().numpy()
        dy1_pre_right = dy1_pre_right.cpu().numpy()
        dy0_pre_wrong = dy0_pre_wrong.cpu().numpy()
        dy1_pre_wrong = dy1_pre_wrong.cpu().numpy()

    # print("\n m_ps_cs_wcl \n", m_ps_cs_wcl)
    # print("\n wst_check_0 \n", wst_check_0)
    # print("\n wst_check_1 \n", wst_check_1)
    # print("\n dy0_pre_right \n", dy0_pre_right)
    # print("\n dy0_pre_wrong \n", dy0_pre_wrong)
    # print("\n dy1_pre_right \n", dy1_pre_right)
    # print("\n dy1_pre_wrong \n", dy1_pre_wrong)
    # print("\n wst_check_w_01 \n", wst_check_w_01)
    # print("\n wst_check_w_01_ij \n", wst_check_w_01_ij)
    # print("\n wst_check_w_10 \n", wst_check_w_10)
    # print("\n wst_check_w_10_ij \n", wst_check_w_10_ij)

    return wst_check_w_01, wst_check_w_10, wst_check_0, wst_check_1, \
           wst_check_w_01_ij, wst_check_w_10_ij, dy0_pre_right, dy0_pre_wrong, dy1_pre_right, dy1_pre_wrong


'''Acc评价模块 —— 包含“考虑类别正确与否” 和 “不考虑类别正确与否” 两种'''
def remove_diag_effect(tensor_in):
    diag = torch.diag(tensor_in)
    e_diag = torch.diag_embed(diag)
    tensor_in = tensor_in - e_diag
    return tensor_in
def statis_rw(numpy_in, r=None, w=None):
    aaa0 = np.argwhere(numpy_in == r)
    bbb0 = np.argwhere(numpy_in == w)
    pre_r = aaa0.shape[0] / 2
    pre_w = bbb0.shape[0] / 2
    return pre_r, pre_w
def statis_rw_c(numpy_in, r=None, w1=None, w2=None):
    aaa0c = np.argwhere(numpy_in == r)
    bbb0c = numpy_in <= w1
    ccc0c = numpy_in >= w2
    temp_bc0c = bbb0c & ccc0c
    temp_0c = np.argwhere(temp_bc0c == True)
    pre_r = aaa0c.shape[0] / 2
    pre_w = temp_0c.shape[0] / 2
    return pre_r, pre_w
def check_acc(p, targets, model, c0_pre_rsum, c0_pre_wsum, c1_pre_rsum, c1_pre_wsum,
              c0_pre_rsum_c, c0_pre_wsum_c, c1_pre_rsum_c, c1_pre_wsum_c):
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    # per output
    nt = 0  # targets
    cp, cn = smooth_BCE(eps=0.0)
    for i, pi in enumerate(p):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp

                '''========================== Meta Learning ================================='''
                if nb > 1:
                    check_ps = ps[:, 5:].detach()
                    check_ps = check_ps.sigmoid()  # sigmoid
                    check_t = t.detach()
                    _, ct = torch.max(check_t, dim=1)
                    _, cps = torch.max(check_ps, dim=1)
                    ct = ct.to(float)
                    cps = cps.to(float)

                    '''
                    pre与label对比mask：
                    -4 —— 2个pre都与各自label不符
                    -1 —— 1个pre都与各自label不符
                    2  —— 2个pre都与各自label相符
                    '''
                    label_pre_check = ct - cps
                    ones_lpc = torch.ones_like(label_pre_check)
                    others_lpc = 0.0 - 2 * ones_lpc
                    label_pre_check = torch.where(label_pre_check == 0.0, ones_lpc, others_lpc)

                    temp_lpc = label_pre_check.unsqueeze(dim=1)
                    temp_lpc = temp_lpc.expand(temp_lpc.shape[0], temp_lpc.shape[0])
                    label_pre_check = label_pre_check + temp_lpc
                    # 去对角线元素影响
                    label_pre_check = remove_diag_effect(label_pre_check)
                    # print('label_pre_check = ', label_pre_check)

                    '''
                    基于label的对比mask：
                    0 —— 自身比较，即对角线，略
                    1 —— 通过label识别，2个label同类
                    10 —— 通过label识别，2个label不同类
                    '''
                    label_sd = torch.norm(check_t[:, None] - check_t, dim=2, p=1) / 2
                    ones_l = torch.ones_like(label_sd)
                    others_l = 10 * ones_l
                    label_sd = torch.where(label_sd > 0.0, others_l, ones_l)
                    # 去对角线元素影响
                    label_sd = remove_diag_effect(label_sd)
                    # print('label_sd = ', label_sd)

                    '''
                    基于pre的对比mask：
                    0 —— 自身比较，即对角线，略
                    1 —— 通过pre识别，2个pre同类
                    2 —— 通过pre识别，2个pre不同类
                    '''
                    temp_cps = cps.unsqueeze(1)
                    pred_sd = torch.norm(temp_cps[:, None] - temp_cps, dim=2, p=1)
                    ones_p = torch.ones_like(pred_sd)
                    others_p = 2 * ones_p
                    pred_sd = torch.where(pred_sd > 0.0, others_p, ones_p)
                    # 去对角线元素影响
                    pred_sd = remove_diag_effect(pred_sd)
                    # print('pred_sd = ', pred_sd)

                    sd_lp_check = label_sd * pred_sd
                    # print('sd_lp_check = ', sd_lp_check)
                    sd_lp_check_n = sd_lp_check.detach().cpu().numpy()
                    c0_pre_r, c0_pre_w = statis_rw(sd_lp_check_n, r=1.0, w=2.0)
                    c1_pre_r, c1_pre_w = statis_rw(sd_lp_check_n, r=20.0, w=10.0)
                    c0_pre_rsum += c0_pre_r
                    c0_pre_wsum += c0_pre_w
                    c1_pre_rsum += c1_pre_r
                    c1_pre_wsum += c1_pre_w
                    # print("\n c0_pre_r = %s\n c0_pre_w = %s\n c1_pre_r = %s\n c1_pre_w = %s"
                    #       % (c0_pre_r, c0_pre_w, c1_pre_r, c1_pre_w))

                    label_pre_check_n = label_pre_check.detach().cpu().numpy()
                    sta_lpc_check = label_pre_check_n * sd_lp_check_n
                    # print('sta_lpc_check = ', sta_lpc_check)
                    c0_pre_rc, c0_pre_wc = statis_rw_c(sta_lpc_check, r=2.0, w1=-1.0, w2=-8.0)
                    c1_pre_rc, c1_pre_wc = statis_rw_c(sta_lpc_check, r=40.0, w1=-10.0, w2=-80.0)
                    c0_pre_rsum_c += c0_pre_rc
                    c0_pre_wsum_c += c0_pre_wc
                    c1_pre_rsum_c += c1_pre_rc
                    c1_pre_wsum_c += c1_pre_wc
                    # print("\n c0_pre_rc = %s\n c0_pre_wc = %s\n c1_pre_rc = %s\n c1_pre_wc = %s"
                    #       % (c0_pre_rc, c0_pre_wc, c1_pre_rc, c1_pre_wc))

    return c0_pre_rsum, c0_pre_wsum, c1_pre_rsum, c1_pre_wsum, c0_pre_rsum_c, c0_pre_wsum_c, c1_pre_rsum_c, c1_pre_wsum_c