import os

wdir = 'weights' + os.sep  # weights dir
last = wdir + 'last.pt'
best = wdir + 'best.pt'
results_file = 'results.txt'
file_cf_means = 'WST_cf_means.txt'
wst_check_w_file = 'wst_check_w.txt'
wst_check_file = 'wst_check_w%.txt'
wst_check_wij_file = 'wst_check_wij.txt'

# Hyperparameters
hyp = {'giou': 3.54,  # giou loss gain
       'cls': 37.4,  # cls loss gain
       'cls_pw': 1.0,  # cls BCELoss positive_weight
       'obj': 64.3,  # obj loss gain (*=img_size/320 if img_size != 320)
       'obj_pw': 1.0,  # obj BCELoss positive_weight
       'iou_t': 0.2,  # iou training threshold
       'lr0': 0.01,  # initial learning rate (SGD=5E-3, Adam=5E-4)
       'lrf': 0.0005,  # final learning rate (with cos scheduler)
       'momentum': 0.937,  # SGD momentum
       'weight_decay': 0.0005,  # optimizer weight decay
       'fl_gamma': 0.0,  # focal loss gamma (efficientDet default is gamma=1.5)
       'hsv_h': 0.0138,  # image HSV-Hue augmentation (fraction)
       'hsv_s': 0.678,  # image HSV-Saturation augmentation (fraction)
       'hsv_v': 0.36,  # image HSV-Value augmentation (fraction)
       'degrees': 1.98 * 0,  # image rotation (+/- deg)
       'translate': 0.05 * 0,  # image translation (+/- fraction)
       'scale': 0.05 * 0,  # image scale (+/- gain)
       'shear': 0.641 * 0}  # image shear (+/- deg)
''' =============== WST Learning: loss gain,positive_weight =============== '''
hyp['wst'] = 4.26
hyp['wst_pw'] = 1.0

# wst_gain for compute loss
wst_gain_1 = 0.8
wst_gain_2 = 0.5

WST_module_name = 'Single'
'''
包含：Single, Same, None
说明：
    Single: WST Head 单独存在
    Same: WST Head 与 yolo Head 同一个
    None: 没有 WST 功能时，原 YOLO
'''

test_module_name = 'v3'
'''
当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
说明：
    v1: L1, L2, Cosine of v1
    v1_cosdis: Cosdis of v1
    v3: L1, L2, Cosine of v3
    v3_cosdis: Cosdis of v3
当 WST Head 与 yolo Head 同一时，包含：s_v1, s_v3
说明：
    s_v1: L1, L2, Cosine of v1
    s_v3: L1, L2, Cosine of v3
'''

wst_loss = 'v3'
'''
无所谓 WST Head 是否单独，包含：v1, v3
说明：
    v1：关键点加权差值方法, 直接差值也在这里, loss_module_name = 'v1' 或 'v1_Cosdis'
    v3：类别均值向量全局差值方法, loss_module_name = 'v3' 或 'v3_Cosdis'
'''

loss_module_name = 'v3'
'''
当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
说明：
    v1: L1, L2, Cosine of v1
    v1_cosdis: Cosdis of v1
    v3: L1, L2, Cosine of v3
    v3_cosdis: Cosdis of v3
当 WST Head 与 yolo Head 同一时，包含：v1, v3
说明：
    v1: L1, L2, Cosine of v1
    v3: L1, L2, Cosine of v3
'''

measure = 'Cosine'
'''
无所谓 WST Head 是否单独，包含：L1, L2, Cosine
说明：
    当 loss_module_name = 'v1_cosdis' 或 'v3_cosdis' 时，measure随意，因为不起作用
'''

BN_WST_module = 'None'
'''
包含：Done, None
说明：
    Done: 在做度量前 Feature maps 做 BN 操作
    None: 在做度量前 Feature maps 不做 BN 操作
'''

OHEM_module = 'None'
'''
包含：Done, None
说明：
    Done: 在做Loss前做 OHEM 难例硬筛选操作
    None: 在做Loss前不做 OHEM 难例硬筛选操作
'''

multi_task_module = 'None'
'''
包含：Done, None
说明：
    Done: 在做WST Loss前做多任务优化操作
    None: 在做WST Loss前不做多任务优化操作
'''

WST_Metric_module = 'Better'

Gradient_Scout_module = 'Done'

Multi_GS_to_WST = 'Done'









